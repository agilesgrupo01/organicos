app.controller("ctrl_session", function ($scope, $http, $rootScope, sessionService) {

    //Variable para consultar si el usuario está autenticado o está autorizado con los roles que se pasan como
    //parámetro
    $scope.session = {};
    $scope.session.authenticated = sessionService.isAuthenticated();
    $scope.session.authorized = sessionService.isAuthorized();

    $scope.session.actualizar = function () {
        $scope.session.authenticated = sessionService.isAuthenticated();
    };

    $scope.session.isAuthorized = function (roles) {
        $scope.session.authorized = sessionService.isAuthorized(roles);
    };

    //Para sacar de sesión al usuario
    $scope.logout = {};

    $scope.logout = function () {
        //localStorage.removeItem("organicos_data");
        localStorage.setItem("organicos_data", "");
        $scope.session.actualizar();
        window.location.href = "/login_view/";
    };

    //Para inicio de sesión
    //$scope.login = {};

    $scope.login = function () {
        $scope.post_login();
    };

    $scope.post_login = function () {
        $http({
            method: "POST",
            url: "/post_login/",
            data: $scope.serialize($scope.login),
            headers: {'Content-Type': "application/x-www-form-urlencoded"}
        })
            .success(function (data, status) {
                localStorage.setItem("organicos_data", JSON.stringify(data));
                $scope.session.actualizar();
                window.location.href = "/";
            }).error(function (data, status) {
            $scope.login = {};
            $("#modal-error").modal("show");
        });
    };

    $scope.serialize = function (obj) {
        var str = [];
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    };

});



