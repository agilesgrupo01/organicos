/**
 * Created by Jorge Barrera on 02/05/2016.
 */

app.controller("sendComplaintController", function ($scope, $route, $routeParams, $location, $http, sessionService) {

    $scope.complaint = {};
    $scope.complaint.copy = true;
    $scope.message = "";

    $scope.sendComplaint = function () {
        $scope.message = "Su mensaje ha sido enviado satisfactoriamente, por favor espere respuesta por correo!";
        $http({
            method: "POST",
            url: "consumers/complaint",
            data: $scope.serialize($scope.complaint),
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        }).success(function (data, status) {
            $scope.message = "Su mensaje ha sido enviado satisfactoriamente, por favor espere respuesta por correo!";
        }).error(function (data, status) {
            $scope.message = "Se presentó un error registrando la información, intente de nuevo\n\n" + data;
        });
    };

    $scope.serialize = function (obj) {
        var str = [];
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        str.push("userId=" + sessionService.getUserId());
        return str.join("&");
    };
});