/**
 * Created by Jorge on 08/03/2016.
 */
app.controller("queryOfferItemReportController", function ($scope, $route, $routeParams, $location, $http, ngTableParams, $log) {

    $scope.allProducts = [];

    $scope.allProducers = {};

    $scope.allOffers = {};

    $scope.selectedOffer = {};

    $scope.offerItemById = {};

    $scope.productById = {};

    $scope.alerts = [];

    $scope.window = {};

    $scope.diasList = [
        {id: 0, name: "Lunes"},
        {id: 1, name: "Martes"},
        {id: 2, name: "Miércoles"},
        {id: 3, name: "Jueves"},
        {id: 4, name: "Viernes"},
        {id: 5, name: "Sábado"},
        {id: 6, name: "Domingo"},
    ];


    $scope.queryAllProducts = function () {

        $http({method: "GET", url: "/product/"})
            .success(function (data, status) {
                $scope.allProducts = data;
                if (data.length > 0) {
                    var last = jQuery.extend(true, {}, data[0]);
                    last.fields.nombre = "-Todos los Productos-";
                    last.pk = 0;
                    $scope.allProducts.push(last);
                }
            }).error(function (data, status) {
            $scope.allProducts = {};
        });
    };

    $scope.queryAllProducers = function () {

        $http({method: "GET", url: "/producers/"})
            .success(function (data, status) {
                $scope.allProducers = data;
            }).error(function (data, status) {
            $scope.allProducers = {};
            $log.error(data);
        });
    };

    $scope.queryAllOffers = function () {

        $http({method: "GET", url: "/offers/"})
            .success(function (data, status) {
                $scope.allOffers = data;
            }).error(function (data, status) {
            $scope.allOffers = {};
            $log.error(data);
        })
    };

    $scope.queryProductById = function (id) {

        var result = {};
        angular.forEach($scope.allProducts, function (value, key) {
            if (value.pk == id) {
                result = value;
            }
        });

        return result;
    };

    $scope.queryProducerById = function (id) {

        var result = {};
        angular.forEach($scope.allProducers, function (value, key) {
            if (value.pk == id) {
                result = value.nombre;
            }
        });

        return result;
    };

    $scope.queryOfferItemsByOffer = function (id) {
        $http({method: "GET", url: "/offerItems/" + id})
            .success(function (data, status) {
                $scope.offerItemById = data;
            }).error(function (data, status) {
            $scope.offerItemById = {};
            $log.error(data);
        })
    };

    $scope.queryOfferItems = function (xfechaInicio, xfechaFin, xidProducto) {

        if (xfechaInicio > xfechaFin) {
            $scope.alerts.push({msg: "¡La fecha de Inicio no puede ser mayor a la fecha Fin!"});
        } else if ((xfechaInicio == undefined && xfechaFin != undefined ) || (xfechaInicio != undefined && xfechaFin == undefined )) {
            $scope.alerts.push({msg: "¡Si selecciona una fecha, tanto la fecha de inicio como la fecha de fin deben estar llenas!"});
        }
        else {

            var f1 = "";
            var f2 = "";
            var idProd = 0;
            if (xfechaInicio != undefined) {
                f1 = xfechaInicio.getFullYear() + "-" + (xfechaInicio.getMonth() + 1) + "-" + xfechaInicio.getDate();
            }
            if (xfechaFin != undefined) {
                f2 = xfechaFin.getFullYear() + "-" + (xfechaFin.getMonth() + 1) + "-" + xfechaFin.getDate();
            }
            if (xidProducto != undefined) {
                idProd = xidProducto;
            }

            var criSearch = {"fechaInicio": f1, "fechaFin": f2, "idProducto": idProd};

            $http({
                method: "POST", url: "/offerItems/report/",
                data: criSearch,
                headers: {"Content-Type": "application/json"}
            })
                .success(function (data, status) {
                    $scope.offerItemById = data;

                    $scope.tableReporte = new ngTableParams(
                        {page: 1, count: 5},
                        {
                            total: $scope.offerItemById.length,
                            getData: function ($defer, params) {

                                $defer.resolve($scope.offerItemById.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                            }
                        }
                    );
                }).error(function (data, status) {
                $scope.offerItemById = {};
                $log.error(data);
            })
        }
    };


    $scope.getValues = function () {
        var values = [];
        angular.forEach($scope.offerItemById, function (value, key) {
            values.push((value.precio * value.cantidadActual));
        });
        return values;
    }

    $scope.getTotalPrice = function () {
        return math.sum($scope.getValues());
    };

    $scope.getTotalMedian = function () {
        return math.median($scope.getValues());
    };

    $scope.getTotalMean = function () {
        return math.mean($scope.getValues());
    };

    $scope.getTotalSTD = function () {
        return math.std($scope.getValues());
    };


    $scope.getWindow = function () {
        $http({method: "GET", url: "/offers/window-value/"})
            .success(function (data, status) {
                $scope.window = data;
            }).error(function (data, status) {
            $scope.window = {};
            $log.error(data);
        });
    };

    $scope.editWindow = function () {
        $http({
            method: "POST", url: "/offers/window-value/",
            data: $scope.window,
            headers: {"Content-Type": "application/json"}
        })
            .success(function (data, status) {
                $scope.window = data;
                $("#modal-window").modal("hide");
            }).error(function (data, status) {
            $scope.errorMessage = data;
            $("#modal-error").modal("show");
            $log.error(data);
        });
    };

    $scope.getDayById = function (id) {
        return $scope.diasList.filter(function (obj) {
            return (obj.id === id);
        })[0].name;
    }

    $scope.queryAllOffers();

    $scope.queryAllProducts();

    $scope.queryAllProducers();


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };


    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.getWindow();


});