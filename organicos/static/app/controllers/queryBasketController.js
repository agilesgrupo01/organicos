/**
 * Created by Lina8a on 09/04/2016.
 */

app.controller("queryBasketController", function ($scope, $http, sessionService, ngTableParams, $log, $filter) {

    $scope.allCategories = {};
    $scope.allBaskets = [];
    $scope.allProducts = [];
    $scope.detailProduct = [];
    $scope.totales = {};
    $scope.basketCategoryId = {};
    $scope.selectedBasketId = {};
    $scope.allItemProducts = [];

    $scope.estadoList = [
        {id: 1, name: "Activo"},
        {id: 0, name: "Inactivo"},
    ];

    $scope.mode = "Agregar";

    $scope.queryAllCategories = function () {
        $http({method: "GET", url: "/categories/"})
            .success(function (data, status) {
                $scope.allCategories = data;
            }).error(function (data, status) {
            $scope.allCategories = {};
            $log.error(data);
        });
    };

    $scope.addEditProduct = function () {
        var send = true;

        if ($scope.newProduct == undefined) {
            send = false;
            $scope.errorMessage = "Debe ingresar información en todos los campos.";
            $("#modal-error").modal("show");
        }

        else {
            if ($scope.newProduct.estado == undefined || $scope.newProduct.nombre == undefined
                || $scope.newProduct.precio == undefined) {
                send = false;
                $scope.errorMessage = "Debe ingresar información en todos los campos.";
                $("#modal-error").modal("show");
            }

            if ($scope.mode === "Agregar") {
                if ($scope.file == undefined) {
                    send = false;
                    $scope.errorMessage = "Debe ingresar información en todos los campos.";
                    $("#modal-error").modal("show");
                }
            }
        }

        if (send) {
            var fd = new FormData();

            if ($scope.mode === "Agregar") {

                fd.append("idCategoria", $scope.basketCategoryId);
                fd.append("estado", $scope.newProduct.estado);
                fd.append("nombre", $scope.newProduct.nombre);
                fd.append("precio", $scope.newProduct.precio);
                fd.append("unidad", "Canasta");
                fd.append("file", $scope.file);

                $http.post("/products/", fd, {
                    withCredentials: true,
                    headers: {"Content-Type": undefined},
                    transformRequest: angular.identity
                }).success(function () {
                    $scope.queryAllBaskets();
                    $("#modal-add").modal("hide");
                }).error(function (e) {
                    $scope.errorMessage = e;
                    $("#modal-error").modal("show");
                });
            }

            else if ($scope.mode === "Actualizar") {

                $log.log($scope.newProduct);
                $log.log($scope.newProduct.idCategoria);

                fd.append("id", $scope.newProduct.id);
                fd.append("idCategoria", $scope.basketCategoryId);
                fd.append("estado", $scope.newProduct.estado);
                fd.append("nombre", $scope.newProduct.nombre);
                fd.append("precio", $scope.newProduct.precio);
                fd.append("unidad", "Canasta");

                if ($scope.file !== undefined) {
                    fd.append("file", $scope.file);
                }

                $http.post("/products/" + $scope.newProduct.id + "/", fd, {
                    withCredentials: true,
                    headers: {"Content-Type": undefined},
                    transformRequest: angular.identity
                }).success(function () {
                        $scope.queryAllBaskets();
                        $("#modal-add").modal("hide");
                    })
                    .error(function (e) {
                        $scope.errorMessage = e;
                        $("#modal-error").modal("show");
                    });
            }
        }
    };

    $scope.getEditProduct = function (id) {
        $scope.mode = "Actualizar";

        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.newProduct = res[0];
                $log.log($scope.newProduct.id);

                if ($scope.newProduct.estado === "Inactivo") {
                    $scope.newProduct.estado = 0;
                }

                else {
                    $scope.newProduct.estado = 1;
                }

                $http({method: "GET", url: "/categories/" + $scope.newProduct.idCategoria + "/"})
                    .success(function (data, status) {
                        $scope.newProduct.idCategoria = data[0].pk;
                    }).error(function (data, status) {
                    $scope.newProduct.idCategoria = {};
                    $log.error(data);
                });

            }).error(function (res, status) {
            $scope.newProduct = {};
            $log.error(res);
        })
    };

    //Trae del servidor la información consolidada de las canastas
    $scope.queryAllBaskets = function () {
        $http({method: "GET", url: "/baskets/"})
            .success(function (res, status) {
                $scope.allBaskets = res;

                $scope.tableProductos = new ngTableParams(
                    {page: 1, count: 3},
                    {
                        total: $scope.allBaskets.length,
                        getData: function ($defer, params) {

                            $defer.resolve($scope.allBaskets.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                        }
                    }
                );

            }).error(function (res, status) {
            $scope.allBaskets = {};
            $log.error(res);
        })
    };

    $scope.queryAllProducts = function () {
        $http({method: "GET", url: "/products/non-basket/"})
            .success(function (res, status) {
                $scope.allProducts = res;
            }).error(function (res, status) {
            $scope.allProducts = {};
            $log.error(res);
        })
    };

    $scope.queryDetailProduct = function (id) {
        $scope.mode = "Detalle";
        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.detailProduct = res;
                $http({method: "GET", url: "/categories/" + $scope.detailProduct[0].idCategoria + "/"})
                    .success(function (data0, status) {
                        $scope.detailProduct[0].categoria = data0[0].fields.nombre;

                        $http({method: "GET", url: "/basketItems/" + id})
                            .success(function (data, status) {

                                $scope.allItemProducts = data;

                                $scope.tableFilter = new ngTableParams(
                                    {page: 1, count: 5},
                                    {
                                        total: data.length,
                                        getData: function ($defer, params) {
                                            // use build-in angular filter
                                            var productData = params.filter() ? $filter("filter")(data, params.filter()) : data;
                                            this.idCategoria = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.estado = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.nombre = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.precio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.fechaRegistro = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.calificacionPromedio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.unidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            this.cantidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                            params.total(productData.length); // set total for recalc pagination
                                            $defer.resolve(this.idCategoria, this.estado, this.nombre, this.precio, this.fechaRegistro, this.calificacionPromedio, this.unidad, this.cantidad);
                                        }
                                    }
                                );


                            }).error(function (data, status) {
                            $scope.allItemProducts = {};
                            $log.error(data);
                        });


                    }).error(function (data0, status) {
                    $scope.detailProduct.categoria = {};
                    $log.error(data0);
                });


            }).error(function (res, status) {
            $scope.detailProduct = {};
            $log.error(res);
        })
    };

    $scope.addBasketItem = function (id) {
        if ($scope.newBasketItem == undefined || $scope.newBasketItem.producto == null || $scope.newBasketItem.cantidad == null) {
            $scope.errorMessage = "Debe ingresar un valor para cada campo.";
            $("#modal-error").modal("show");
        }

        else {
            if ($scope.newBasketItem.cantidad == 0) {
                $scope.errorMessage = "La cantidad del producto debe ser mayor a 0.";
                $("#modal-error").modal("show");
            }

            else {
                $scope.newBasketItem.idCanasta = $scope.selectedBasketId;
                $scope.newBasketItem.idProducto = $scope.newBasketItem.producto.pk;
                $http({
                    method: "POST", url: "basketItems/",
                    data: $scope.newBasketItem,
                    headers: {"Content-Type": "application/json"}
                })
                    .success(function (data, status) {
                        $("#modal-add-item").modal("hide");
                        $scope.newBasketItem = {};
                        $scope.queryDetailProduct($scope.selectedBasketId);
                    }).error(function (data, status) {
                    $scope.errorMessage = data;
                    $("#modal-error").modal("show");
                    $scope.newBasketItem = {};
                });
            }
        }
    };

    $scope.allowBasketDelete = function (id) {
        $scope.basketDeleteID = id;
    };

    $scope.deleteBasketById = function () {
        if ($scope.basketDeleteID != null) {
            $http({method: "DELETE", url: "/products/" + $scope.basketDeleteID})
                .success(function (data, status) {
                    $("#modal-delete").modal("hide");
                    $scope.queryAllBaskets();
                }).error(function (data, status) {
                $log.error(data);
            })
        }
    };

    $scope.queryBasketCategoryId = function () {
        $http({method: "GET", url: "/baskets/category/"})
            .success(function (data, status) {
                $scope.basketCategoryId = data;
            }).error(function (data, status) {
            $scope.basketCategoryId = {};
        });
    };

    $scope.setSelectedBasket = function (id) {
        $scope.selectedBasketId = id;
    };

    $scope.setMode = function (modo) {
        $scope.mode = modo;
        return $scope.mode;
    };

    $scope.createIdByName = function (name) {
        return name.trim().replace(" ", "-");
    };

    $scope.queryBasketCategoryId();

    $scope.queryAllBaskets();

    $scope.queryAllCategories();

    $scope.queryAllProducts();
});