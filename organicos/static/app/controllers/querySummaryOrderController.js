/**
 * Created by jm.soto569 on 28/03/2016.
 */

app.controller("querySummaryOrderController", function ($scope, $route, $routeParams, $location, $http, sessionService, $log) {

    $scope.allProducts = [];
    $scope.totales = {};

    //Trae del servidor la información consolidada del pedido y las ordenes que se han hecho a este
    $scope.queryOrderSummary = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "/offers/summary/" + id + "/"})
            .success(function (data, status) {
                $scope.allProducts = [];
                $scope.totales = {};
                var long = data.length;
                if (long > 3) {
                    for (var i = 0; i < long - 3; i++) {
                        $scope.allProducts.push(data[i]);
                    }
                    $scope.totales = {"ofertado": data[long - 3], "disponible": data[long - 2], "solicitado": data[long - 1]};
                }
            }).error(function (data, status) {
            $scope.allProducts = {};
            $log.error(data);
        });
    };

    $scope.queryOrderSummary();

});