/**
 * Created by Juan Manuel
 */
app.controller("queryFranjaController", function ($scope, $route, $routeParams, $location, $http, sessionService, $log) {

    //Estados de una oferta

    $scope.franjasActuales = {};

    $scope.nuevaFranja = {};

    $scope.franjaIdBorrar = -1;

    //Consulta la franja de la siguiente semana
    $scope.queryUltimaFranja = function () {

        $http({method: "GET", url: "/franjas/actuales/"})
            .success(function (data, status) {
                $scope.franjasActuales = data;
            }).error(function (data, status) {
            $scope.franjasActuales = {};
            $log.error(data);
        });
    };

    $scope.setFranjaBorrar = function (id) {
        $log.info("Id a borrar: " + id);
        $scope.franjaIdBorrar = id;
    };

    $scope.deleteFranja = function () {
        $log.info("Id a borrar delete function: " + $scope.franjaIdBorrar);
        $http({method: "DELETE", url: "/franjas/delete/" + $scope.franjaIdBorrar + "/"})
            .success(function (data, status) {
                $scope.queryUltimaFranja();

            }).error(function (data, status) {
            $log.error("Error borrando elemento");
        });
    };

    //Añade un item nuevo a la oferta o actualiza los datos de uno existente dependiendo del modo en que se encuentre
    $scope.addFranja = function () {
        $log.log($scope.nuevaFranja);
        $http({
            method: "POST", url: "franjas/new/",
            data: $scope.nuevaFranja,
            headers: {"Content-Type": "application/json"}
        })
            .success(function (data, status) {
                $scope.queryUltimaFranja();
                $("#modal-add").modal("hide");
            }).error(function (data, status) {
            $scope.errorMessage = data;
            $log.log(data);
            $("#modal-error").modal("show");
            $log.error(data);
        });
    };

    $scope.queryUltimaFranja();

});