/**
 * Created by Jorge on 08/03/2016.
 */
app.controller("queryProducersController", function ($scope, $route, $routeParams, $location, $http) {

    $scope.allProducers = {};
    $scope.producer = {};
    $scope.fotoCompressed = {};
    $scope.message = "";

    //Pager
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.itemsPerPage = 2;

    $scope.queryAllProducers = function () {

        $http({method: "GET", url: "/producers/"})
            .success(function (data, status) {
                $scope.allProducers = angular.copy(data);
                $scope.totalItems = $scope.allProducers.length;
            })
            .error(function (data, status) {
                $scope.allProducers = {};
                $scope.totalItems = 0;
            });
    };

    $scope.showDetail = function (item) {
        $scope.producer = item;
        $("#producer").modal("show");
    };

    $scope.showCreate = function () {
        $scope.producer = {};
        $scope.producer.pk = 0;
        $("#producer").modal("show");
    };

    $scope.createProducer = function () {

        $scope.message = "Se registró satisfactoriamente la información";
        if ($scope.fotoCompressed.compressed.dataURL === "undefined") {
            $scope.message = "Selecciona una imagen";
        } else {
            $scope.producer.foto = $scope.fotoCompressed.compressed.dataURL;
            $http({
                method: "POST",
                url: "/producers/add/",
                data: $scope.serialize($scope.producer),
                headers: {"Content-Type": "application/x-www-form-urlencoded"}
            }).success(function (data, status) {
                $scope.message = "Se registró satisfactoriamente la información";
                $scope.queryAllProducers();
                $("#producer").modal("hide");
            }).error(function (data, status) {
                $scope.message = "Se presentó un error registrando la información, intente de nuevo\n\n" + data;
            });
        }
    };

    $scope.serialize = function (obj) {
        var str = [];
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    };

    $scope.queryAllProducers();

});