/**
 * Created by Lina8a on 02/05/2016.
 */

app.controller("registerController", function ($scope, $route, $routeParams, $location, $http, sessionService, $log) {

    $scope.addConsumer = function () {
        var send = true;

        if ($scope.newConsumer === undefined) {
            send = false;
            $scope.errorMessage = "Debe ingresar información en los campos requeridos.";
            $("#modal-error").modal("show");
        }

        else {
            if ($scope.newConsumer.nombres === undefined || $scope.newConsumer.apellidos === undefined
                || $scope.newConsumer.telefono === undefined || $scope.newConsumer.direccion === undefined
                || $scope.newConsumer.correoElectronico === undefined || $scope.newConsumer.contrasenia === undefined) {
                send = false;
                $log.log($scope.newConsumer.nombres);
                $log.log($scope.newConsumer.apellidos);
                $log.log($scope.newConsumer.telefono);
                $log.log($scope.newConsumer.direccion);
                $log.log($scope.newConsumer.correoElectronico);
                $log.log($scope.newConsumer.contrasenia);
                $scope.errorMessage = "Debe ingresar información en todos los campos.";
                $("#modal-error").modal("show");
            }
            else {
                if ($scope.formRegister.correoElectronico.$valid == false) {
                    $scope.errorMessage = "Ingrese un correo electrónico válido.";
                    $("#modal-error").modal("show");
                }
            }
        }

        if (send) {
            var fd = new FormData();
            fd.append("nombres", $scope.newConsumer.nombres);
            fd.append("apellidos", $scope.newConsumer.apellidos);
            fd.append("telefono", $scope.newConsumer.telefono);
            fd.append("direccion", $scope.newConsumer.direccion);
            fd.append("correoElectronico", $scope.newConsumer.correoElectronico);
            fd.append("contrasenia", $scope.newConsumer.contrasenia);

            if ($scope.file != undefined) {
                fd.append("file", $scope.file);
            }

            $http.post("/consumers/", fd, {
                withCredentials: true,
                headers: {"Content-Type": undefined},
                transformRequest: angular.identity
            }).success(function () {
                window.location.href = "/login_view/";
            }).error(function (e) {
                $scope.errorMessage = e;
                $("#modal-error").modal("show");
            });
        }
    };
});