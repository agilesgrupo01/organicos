/**
 * Created by jd.runza on 09/03/2016.
 */

app.controller("queryOrderController", function ($scope, $http, sessionService, $filter, ngTableParams, $log) {

    $scope.allProducts = {};
    $scope.allOrders = {};
    $scope.detailOrder = {};
    $scope.newOrderItem = {};
    $scope.detailProduct = [];
    $scope.detailProductCanasta =[];
    $scope.allItemProductsCanasta =[];

    $scope.mode = "Agregar";
    $scope.idOrder = 0;
    $scope.errorCompraDesc = [];
    $scope.errorCompra = false;
    $scope.showProd = 0;
    //Atributos para seleccion de franja
    $scope.diasDisponibles = [];
    //Franjas disponibles devueltas por el servicio
    $scope.franjas = [];
    $scope.diaSeleccionado = "";
    $scope.franjasMostrar = [];
    $scope.franjaSeleccionada = {};
    $scope.camposValidos = false;
    $scope.lastProd = "";


    $scope.queryAllProducts = function () {
        $http({method: "GET", url: "product/filter/"})
            .success(function (data, status) {
                $scope.allProducts = data;

                $scope.tableFilter = new ngTableParams(
                    {page: 1, count: 5},
                    {
                        total: data.length,
                        getData: function ($defer, params) {
                            // use build-in angular filter
                            var productData = params.filter() ? $filter("filter")(data, params.filter()) : data;
                            this.idCategoria = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.estado = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.nombre = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.precio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.fechaRegistro = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.calificacionPromedio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.unidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.cantidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            params.total(productData.length); // set total for recalc pagination
                            $defer.resolve(this.idCategoria, this.estado, this.nombre, this.precio, this.fechaRegistro, this.calificacionPromedio, this.unidad, this.cantidad);
                        }
                    }
                );

            }).error(function (data, status) {
            $scope.allProducts = {};
            $log.error(data);
        });
    };

    $scope.queryCurrentOrder = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "/orders/" + id})
            .success(function (data, status) {
                $scope.allOrders = data;
                $scope.idOrder = data[0].pk;
                var idFranja = data[0].fields.idFranja;
                if (data[0].fields.estado == 1 && idFranja && idFranja > 0) {
                    $scope.franjaSeleccionada = $scope.queryFranja(idFranja);
                }
                $scope.queryDetailOrder($scope.idOrder);
            }).error(function (data, status) {
            $scope.service = {};
        });
    };

    $scope.queryFranja = function (idFranja) {
        $http({method: "GET", url: "/orders/franja/" + idFranja + "/"})
            .success(function (data, status) {
                $scope.franjaSeleccionada = data[0];
            }).error(function (data, status) {
        })
    };

    $scope.queryDetailOrder = function (id) {
        $http({method: "GET", url: "/orders/detail/" + id})
            .success(function (data, status) {
                $scope.detailOrder = data;
            }).error(function (data, status) {
            $scope.service = {};
        })
    };

    $scope.deleteOrderItemById = function (id) {
        $log.info("Borrando item: " + id);
        $http({method: "GET", url: "/orderItem/detail/" + id})
            .success(function (data, status) {
                $scope.detailOrder = data;
                $scope.queryAllOrders();
            }).error(function (data, status) {
            $scope.service = {};
            alert(data);
        })
    };

    $scope.buyOrder = function () {
        $log.info("Franja seleccionada: " + $scope.franjaSeleccionada.id);
        $http({method: "GET", url: "/consumers/orders/buy/" + this.idOrder + "/" + $scope.franjaSeleccionada.id + "/"})
            .success(function (data, status) {
                $scope.errorCompra = false;
                $scope.errorCompraDesc = [];
                $scope.allOrders[0].fields.estado = 1;
                $("#modal-buy").modal("hide");
            }).error(function (data, status) {
            $scope.errorCompra = true;
            $scope.errorCompraDesc = data;
            $("#modal-buy").modal("hide");
        })
    };

    $scope.getFranjaSeleccionada = function () {
        allOrders[0].idFranja;
    };

    $scope.consultarFranjas = function () {
        $http({method: "GET", url: "/orders/franjas/"})
            .success(function (data, status) {
                if (data.length > 1) {
                    $scope.franjas = [];
                    $scope.diasDisponibles = data[0].diasDisponibles;
                    for (var i = 1; i < data.length; i++) {
                        $scope.franjas.push(data[i]);
                    }
                }
            }).error(function (data, status) {
            $log.error(data);
        })
    };

    //Actualiza las franjas dependiendo de la selección del día realizada por el usuario
    $scope.actualizarFranjasMostrar = function (diaSeleccionado) {
        $log.info("Dia seleccionado: " + diaSeleccionado);
        $scope.franjasMostrar = [];
        for (var i = 0; i < $scope.franjas.length; i++) {
            if ($scope.franjas[i].dia == diaSeleccionado) {
                $scope.franjasMostrar.push($scope.franjas[i]);
            }
        }
    };

    //Valida si hay más de una franja disponible y si el usuario ha seleccionado una de ellas
    $scope.validarCampos = function () {
        $log.info("Validando campos: ");
        if ($scope.franjas.length <= 0 || !$scope.franjaSeleccionada.id) {
            $scope.camposValidos = false;
        } else {
            $scope.camposValidos = true;
        }
    }

    $scope.addOrderItem = function () {
        if ($scope.mode === "Agregar") {
            $log.info("Pedido a añadir:" + $scope.idOrder)
            if (!$scope.newOrderItem.producto || $scope.newOrderItem.cantidad == null) {
                $scope.errorMessage = "Debe ingresar un valor para cada campo.";
                $("#modal-error").modal("show");
            } else if ($scope.newOrderItem.producto.cantidad < $scope.newOrderItem.cantidad) {
                $scope.errorMessage = "Actualmente no tenemos unidades disponibles de este producto, intenta la siguiente semana o disminuye la cantidad.";
                $("#modal-error").modal("show");
            } else {
                $scope.newOrderItem.idPedido = $scope.idOrder;
                $scope.newOrderItem.idProducto = $scope.newOrderItem.producto.id;
                $scope.newOrderItem.precioProducto = $scope.newOrderItem.producto.precio;
                $http({
                    method: "POST", url: "orderItems/",
                    data: $scope.newOrderItem,
                    headers: {"Content-Type": "application/json"}
                })
                    .success(function (data, status) {
                        $scope.queryDetailOrder($scope.idOrder);
                        $("#modal-add").modal("hide");
                        $scope.queryAllOrders();
                        $scope.showProd = 1;
                    }).error(function (data, status) {
                    $scope.errorMessage = data;
                    $("#modal-error").modal("show");
                    $log.error(data);
                });
            }
        } else {
            $log.info("Actualizando item: " + $scope.newOrderItem.id);
            $http({
                method: "POST", url: "orderItems/item/" + $scope.newOrderItem.id,
                data: $scope.newOrderItem,
                headers: {"Content-Type": "application/json"}
            })
                .success(function (data, status) {
                    $scope.queryDetailOrder($scope.idOrder);
                    $scope.queryAllOrders();
                    $("#modal-add").modal("hide");
                }).error(function (data, status) {
            });
        }
    };

    $scope.setMode = function (modo) {
        $scope.mode = modo;
        return $scope.mode;
    };

    $scope.setVisProd = function (modo) {
        $scope.showProd = modo;
        return $scope.showProd;
    };


    $scope.queryDetailProduct = function (id) {
        $scope.mode = "Detalle";
        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.detailProduct = res;
                $http({method: "GET", url: "/categories/" + $scope.detailProduct[0].idCategoria + "/"})
                    .success(function (data, status) {
                        $scope.detailProduct[0].categoria = data[0].fields.nombre;
                    }).error(function (data, status) {
                    $scope.detailProduct.categoria = {};
                    $log.error(data);
                });


            }).error(function (res, status) {
            $scope.detailProduct = {};
            $log.error(res);
        })
    };

    $scope.updateOrderItemById = function (id) {
        $scope.setMode("Actualizar");
        $scope.queryItemFromOrderById(id);
    };

    $scope.queryItemFromOrderById = function (id) {
        $http({method: "GET", url: "/orderItems/item/" + id})
            .success(function (data, status) {
                $log.info("Orden consultada" + data[0]);
                $scope.newOrderItem = data[0];
            }).error(function (data, status) {
            $log.error("Error al consultar item de la orden");
        })
    };

    $scope.setNewOrderItem = function (producto) {
        if (document.getElementById($scope.lastProd) != null) {
            document.getElementById($scope.lastProd).style.backgroundColor = "white";
        }
        document.getElementById(producto.nombre).style.backgroundColor = "lightgray";
        $scope.newOrderItem.producto = producto;
        $scope.newOrderItem.nombreProducto = producto.nombre;
        $scope.lastProd = producto.nombre;
    };


    $scope.queryDetailProductCanasta = function (id) {
        $("#modal-canasta").modal("show");
        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.detailProductCanasta = res;
                $http({method: "GET", url: "/categories/" + $scope.detailProductCanasta[0].idCategoria + "/"})
                    .success(function (data0, status) {
                        $scope.detailProductCanasta[0].categoria = data0[0].fields.nombre;
                        if ($scope.detailProductCanasta[0].categoria== "Canasta")
                            $http({method: "GET", url: "/basketItems/" + id})
                                .success(function (data, status) {
                                    $scope.allItemProductsCanasta = data;
                                    $scope.tableFilterCanasta = new ngTableParams(
                                        {page: 1, count: 5},
                                        {
                                            total: data.length,
                                            getData: function ($defer, params) {
                                                // use build-in angular filter
                                                var productData = params.filter() ? $filter("filter")(data, params.filter()) : data;
                                                this.idCategoria = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.estado = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.nombre = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.precio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.fechaRegistro = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.calificacionPromedio = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.unidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                this.cantidad = productData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                                params.total(productData.length); // set total for recalc pagination
                                                $defer.resolve(this.idCategoria, this.estado, this.nombre, this.precio, this.fechaRegistro, this.calificacionPromedio, this.unidad, this.cantidad);
                                            }
                                        }
                                    );



                                }).error(function (data, status) {
                                $scope.allItemProductsCanasta  = {};
                                $log.error(data);
                            });


                    }).error(function (data0, status) {
                    $scope.detailProduct.categoria = {};
                    $log.error(data0);
                });


            }).error(function (res, status) {
            $scope.detailProduct = {};
            $log.error(res);
        })
    };


    $scope.queryCurrentOrder();
    $scope.queryAllProducts();
    $scope.consultarFranjas();

});