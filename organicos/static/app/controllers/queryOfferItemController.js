/**
 * Created by Jorge on 08/03/2016.
 */
app.controller("queryOfferItemController", function ($scope, $route, $routeParams, $location, $http, sessionService, $log) {

    //Estados de una oferta
    //-1: no se ha definido si se confirma o cancela
    //0: se canceló
    //1: se confirmo

    $scope.allProducts = {};

    $scope.offersByProducer = {};

    $scope.selectedOffer = {};

    $scope.offerItemById = {};

    $scope.productById = {};

    //Oferta actualmente seleccionada por el usuario
    $scope.currentOffer = {};

    $scope.newOfferItem = {};

    //Define si la oferta seleccionada actualmente ya ha sido confirmada
    $scope.isLatestOfferSelected = false;

    $scope.isCurrentOfferEditable = false;

    $scope.latestOfferState = false;

    //Variable para identificar si el pop up es de edición o para agregar un nuevo item
    //Los dos posibles valores son "Agregar" o "Actualizar"
    $scope.mode = "Agregar";

    $scope.queryAllProducts = function () {

        $http({method: "GET", url: "/products/non-basket/"})
            .success(function (data, status) {
                $scope.allProducts = data;
            }).error(function (data, status) {
            $scope.allProducts = {};
            $log.error(data);
        });
    };

    //Se consultan todas las ofertas asociadas al productor
    //Si para el periodo actual no se ha creado ninguna oferta se crea
    $scope.queryOffersByProducer = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "/offers/producers/" + id})
            .success(function (data, status) {
                $scope.allOffers = data;
                $scope.queryCurrentOffer();
            }).error(function (data, status) {
            $scope.allOffers = {};
            $log.error(data);
        })
    };

    $scope.queryProductById = function (id) {
        var result = {};
        angular.forEach($scope.allProducts, function (value, key) {
            if (value.pk == id) {
                result = value.fields;
            }
        });

        return result;
    };

    $scope.queryOfferItemsByOffer = function (id) {
        $http({method: "GET", url: "/offerItems/" + id})
            .success(function (data, status) {
                $scope.offerItemById = data;
                if ($scope.currentOffer && $scope.selectedOffer.pk == $scope.currentOffer.pk) {
                    $scope.isLatestOfferSelected = true;
                }
                else {
                    $scope.isLatestOfferSelected = false;
                }
                $log.log("Es editable la oferta: " + ($scope.latestOfferState == -1 && $scope.isLatestOfferSelected));
                $scope.isCurrentOfferEditable = $scope.latestOfferState == -1 && $scope.isLatestOfferSelected;
            }).error(function (data, status) {
            $scope.offerItemById = {};
            $log.error(data);
        })
    };

    $scope.allowOfferItemDelete = function (id) {
        $scope.offerItemDeleteID = id;
    };

    $scope.deleteOfferItemById = function () {
        if ($scope.offerItemDeleteID != null) {
            $http({method: "DELETE", url: "/offerItems/" + $scope.offerItemDeleteID})
                .success(function (data, status) {
                    for (var i = 0; i < $scope.offerItemById.length; i++) {
                        if ($scope.offerItemById[i].pk == $scope.offerItemDeleteID) {
                            $scope.offerItemById.splice(i, 1);
                        }
                    }

                    $scope.offerItemDeleteID = null;
                }).error(function (data, status) {
                $log.error(data);
            })
        }
    };

    $scope.updateOfferItemById = function (id) {
        $scope.setMode("Actualizar");
        $scope.queryItemFromOfferById(id);
    };

    //Se verifica que se este entre la franja de tiempo definida para confirmar la oferta
    $scope.queryOfferWindow = function () {
        $http({method: "GET", url: "/offers/window/"})
            .success(function (data, status) {
                $log.log(data);
                if (data == "true") {
                    $scope.offerWindow = true;
                }
                else {
                    $scope.offerWindow = false;
                }
            }).error(function (data, status) {
            $log.error(data);
        })
    };


    $scope.confirmOffer = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "/producers/" + id + "/offers/1/"})
            .success(function (data, status) {
                $scope.latestOfferState = 1;
                $scope.isCurrentOfferEditable = false;
            }).error(function (data, status) {
            $log.error(data);
        })
    };

    $scope.rejectOffer = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "/producers/" + id + "/offers/0/"})
            .success(function (data, status) {
                $scope.latestOfferState = 0;
                $scope.isCurrentOfferEditable = false;
                $log.log("Oferta cancelada exitosamente");
            }).error(function (data, status) {
        })
    };

    //Consulta la ultima oferta creada por el productor
    $scope.queryCurrentOffer = function () {
        var id = sessionService.getUserId();
        $http({method: "GET", url: "producers/" + id + "/offers/latest/"})
            .success(function (data, status) {
                if (data) {
                    $log.log("Data: " + data);
                    $log.log("Data[0]: " + data[0]);
                    $log.log("Se encontro oferta actual");
                    $scope.currentOffer = data[0];
                    $scope.latestOfferState = data[0].fields.estado;
                } else {
                    $log.log("NO Se encontro oferta actual");
                    $scope.currentOffer = false;
                }
            }).error(function (data, status) {
            $log.error("ERROR - Consultando oferta actual");
            $scope.currentOffer = {};
        });
    };

    //Añade un item nuevo a la oferta o actualiza los datos de uno existente dependiendo del modo en que se encuentre
    $scope.addOfferItem = function () {
        if ($scope.mode === "Agregar") {

            if (!$scope.newOfferItem.producto || $scope.newOfferItem.precio == null || $scope.newOfferItem.cantidadOrigen == null) {
                $scope.errorMessage = "Debe ingresar un valor para cada campo.";
                $("#modal-error").modal("show");
            }

            else {
                $scope.newOfferItem.idOferta = $scope.currentOffer.pk;
                $scope.newOfferItem.idProducto = $scope.newOfferItem.producto.pk;
                $http({
                    method: "POST", url: "offerItems/",
                    data: $scope.newOfferItem,
                    headers: {"Content-Type": "application/json"}
                })
                    .success(function (data, status) {
                        $scope.offerItemById.push(data[0]);
                        $("#modal-add").modal("hide");
                        $scope.newOfferItem = {};
                    }).error(function (data, status) {
                    $scope.errorMessage = data;
                    $("#modal-error").modal("show");
                    $scope.newOfferItem = {};
                });
            }

        } else {
            $http({
                method: "POST", url: "offerItems/item/" + $scope.newOfferItem.id,
                data: $scope.newOfferItem,
                headers: {"Content-Type": "application/json"}
            })
                .success(function (data, status) {
                    for (var item in $scope.offerItemById) {
                        if ($scope.offerItemById[item].pk == data[0].pk) {
                            $scope.offerItemById[item] = data[0];
                            break;
                        }
                    }
                    $("#modal-add").modal("hide");
                }).error(function (data, status) {
                $log.error(data);
            });
        }

    };

    $scope.queryItemFromOfferById = function (id) {
        $http({method: "GET", url: "/offerItems/item/" + id})
            .success(function (data, status) {
                $scope.newOfferItem = data[0];
            }).error(function (data, status) {
            $log.error("Error al consultar item de la oferta: " + data);
        })
    };

    $scope.setMode = function (modo) {
        $scope.newOfferItem = {};
        $scope.mode = modo;
        return $scope.mode;
    };

    $scope.queryOfferWindow();

    $scope.queryOffersByProducer();

    $scope.queryAllProducts();


});