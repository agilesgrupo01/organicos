/**
 * Created by Lina8a on 09/04/2016.
 */

app.controller("queryProductController", function ($scope, $http, sessionService, ngTableParams, $log) {

    $scope.allCategories = {};
    $scope.allProducts = [];
    $scope.detailProduct = [];
    $scope.totales = {};
    $scope.basketCategoryId = {};

    $scope.estadoList = [
        {id: 1, name: "Activo"},
        {id: 0, name: "Inactivo"},
    ];

    $scope.mode = "Agregar";

    $scope.queryAllCategories = function () {
        $http({method: "GET", url: "/categories/"})
            .success(function (data, status) {
                $scope.allCategories = data;
            }).error(function (data, status) {
            $scope.allCategories = {};
            $log.error(data);
        });
    };

    $scope.addEditProduct = function () {
        var send = true;

        if ($scope.newProduct === undefined) {
            send = false;
            $scope.errorMessage = "Debe ingresar información en todos los campos.";
            $("#modal-error").modal("show");
        }

        else {
            if ($scope.newProduct.idCategoria === undefined || $scope.newProduct.estado === undefined
                || $scope.newProduct.nombre === undefined || $scope.newProduct.precio === undefined
                || $scope.newProduct.unidad === undefined) {
                send = false;
                $scope.errorMessage = "Debe ingresar información en todos los campos.";
                $("#modal-error").modal("show");
            }

            if ($scope.mode === "Agregar") {
                if ($scope.file == undefined) {
                    send = false;
                    $scope.errorMessage = "Debe ingresar información en todos los campos."
                    $("#modal-error").modal("show");
                }
            }
        }

        if ($scope.newProduct.idCategoria != $scope.basketCategoryId &&
            $scope.newProduct.precio <= 0) {
            send = false;
            $scope.errorMessage = "El precio debe ser mayor a cero.";
            $("#modal-error").modal("show");
        }

        if (send) {
            var fd = new FormData();

            if ($scope.mode === "Agregar") {

                fd.append("idCategoria", $scope.newProduct.idCategoria);
                fd.append("estado", $scope.newProduct.estado);
                fd.append("nombre", $scope.newProduct.nombre);
                fd.append("precio", $scope.newProduct.precio);
                fd.append("unidad", $scope.newProduct.unidad);
                fd.append("file", $scope.file);

                $http.post("/products/", fd, {
                    withCredentials: true,
                    headers: {"Content-Type": undefined},
                    transformRequest: angular.identity
                }).success(function () {
                    $scope.queryAllProducts();
                    $("#modal-add").modal("hide");
                }).error(function (e) {
                    $scope.errorMessage = e;
                    $("#modal-error").modal("show");
                });
            }

            else if ($scope.mode === "Actualizar") {
                $log.log($scope.newProduct);
                $log.log($scope.newProduct.idCategoria);

                fd.append("id", $scope.newProduct.id);
                fd.append("idCategoria", $scope.newProduct.idCategoria);
                fd.append("estado", $scope.newProduct.estado);
                fd.append("nombre", $scope.newProduct.nombre);
                fd.append("precio", $scope.newProduct.precio);
                fd.append("unidad", $scope.newProduct.unidad);

                if ($scope.file != undefined) {
                    fd.append("file", $scope.file);
                }

                $http.post("/products/" + $scope.newProduct.id + "/", fd, {
                    withCredentials: true,
                    headers: {"Content-Type": undefined},
                    transformRequest: angular.identity
                }).success(function () {
                        $scope.queryAllProducts();
                        $("#modal-add").modal("hide");
                    })
                    .error(function (e) {
                        $scope.errorMessage = e;
                        $("#modal-error").modal("show");
                    });
            }
        }
    };

    $scope.getEditProduct = function (id) {
        $scope.mode = "Actualizar";

        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.newProduct = res[0];
                $log.log($scope.newProduct.id);

                if ($scope.newProduct.estado === "Inactivo") {
                    $scope.newProduct.estado = 0;
                }

                else {
                    $scope.newProduct.estado = 1;
                }

                $http({method: "GET", url: "/categories/" + $scope.newProduct.idCategoria + "/"})
                    .success(function (data, status) {
                        $scope.newProduct.idCategoria = data[0].pk;
                    }).error(function (data, status) {
                    $scope.newProduct.idCategoria = {};
                    $log.error(data);
                });

            }).error(function (res, status) {
            $scope.newProduct = {};
            $log.error(res);
        })
    };

    //Trae del servidor la información consolidada de los productos
    $scope.queryAllProducts = function () {
        $http({method: "GET", url: "/products/non-basket/"})
            .success(function (res, status) {
                $scope.allProducts = res;

                $scope.tableProductos = new ngTableParams(
                    {page: 1, count: 3},
                    {
                        total: $scope.allProducts.length,
                        getData: function ($defer, params) {

                            $defer.resolve($scope.allProducts.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                        }
                    }
                );

            }).error(function (res, status) {
            $scope.allProducts = {};
            $log.error(res);
        })
    };

    $scope.queryDetailProduct = function (id) {
        $scope.mode = "Detalle";
        $http({method: "GET", url: "/product/" + id})
            .success(function (res, status) {
                $scope.detailProduct = res;
                $http({method: "GET", url: "/categories/" + $scope.detailProduct[0].idCategoria + "/"})
                    .success(function (data, status) {
                        $scope.detailProduct[0].categoria = data[0].fields.nombre;
                    }).error(function (data, status) {
                    $scope.detailProduct.categoria = {};
                    $log.error(data);
                });


            }).error(function (res, status) {
            $scope.detailProduct = {};
            $log.error(res);
        })
    };

    $scope.allowProductDelete = function (id) {
        $scope.productDeleteID = id;
    };

    $scope.deleteProductById = function () {
        if ($scope.productDeleteID != null) {
            $http({method: "DELETE", url: "/products/" + $scope.productDeleteID})
                .success(function (data, status) {
                    $("#modal-delete").modal("hide");
                    $scope.queryAllProducts();
                }).error(function (data, status) {
                $log.error(data);
            })
        }
    };

    $scope.queryBasketCategoryId = function () {
        $http({method: "GET", url: "/baskets/category/"})
            .success(function (data, status) {
                $scope.basketCategoryId = data;
            }).error(function (data, status) {
            $scope.basketCategoryId = {};
        });
    };

    $scope.setMode = function (modo) {
        $scope.mode = modo;
        return $scope.mode;
    };

    $scope.createIdByName = function (name) {
        return name.trim().replace(" ", "-");
    };

    $scope.queryBasketCategoryId();

    $scope.queryAllProducts();

    $scope.queryAllCategories();
});