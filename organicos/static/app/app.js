/**
 * Created by Jorge Barrera on 15/02/2016.
 */
var app = angular.module("organicos", ["ngRoute", "ngTable", "ui.bootstrap"]).config(function ($routeProvider, $locationProvider, $httpProvider, $interpolateProvider) {


    /* Disable Cache */
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
    $httpProvider.defaults.headers.get["If-Modified-Since"] = "0";

    /* Change Tags */
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");

    /* Route Views */
    /* https://docs.angularjs.org/api/ngRoute/service/$route */
    $routeProvider

        .when("/inicio", {
            templateUrl: "/static/app/views/indexView.html"
        })

        .when("/login_view", {
            templateUrl: "/static/app/views/logInView.html"
        })

        .when("/registro", {
            templateUrl: "/static/app/views/registerView.html",
            controller: "registerController"
        })

        .when("/ofertas", {
            templateUrl: "/static/app/views/queryOfferItemView.html",
            controller: "queryOfferItemController"
        })

        .when("/reporte-ofertas", {
            templateUrl: "/static/app/views/queryOfferItemReportView.html",
            controller: "queryOfferItemReportController"
        })

        .when("/pedidos", {
            templateUrl: "/static/app/views/queryOrderView.html",
            controller: "queryOrderController"
        })

        .when("/summary-pedidos", {
            templateUrl: "/static/app/views/querySummaryOrderView.html",
            controller: "querySummaryOrderController"
        })

        .when("/productos", {
            templateUrl: "/static/app/views/queryProductView.html",
            controller: "queryProductController"
        })

        .when("/canastas", {
            templateUrl: "/static/app/views/queryBasketView.html",
            controller: "queryBasketController"
        })

        .when("/productores", {
            templateUrl: "/static/app/views/queryProducersView.html",
            controller: "queryProducersController"
        })

        .when("/franjas", {
            templateUrl: "/static/app/views/queryFranjaView.html",
            controller: "queryFranjaController"
        })

        .when("/complaints", {
            templateUrl: "/static/app/views/sendComplaintView.html",
            controller: "sendComplaintController"
        })

        .otherwise({redirectTo: "/inicio"});

    $(".consumidor").hide();
    $(".productor").hide();
    $(".administrador").hide();

    if (getUserRole() === "Consumidor") {
        $(".consumidor").show();
    } else if (getUserRole() === "Productor") {
        $(".productor").show();
    } else if (getUserRole() === "Administrador") {
        $(".administrador").show();
    }
});

// Tomado del blog de Cheyne Wallace "Uploading To S3 With AngularJS", disponible en:
// http://www.cheynewallace.com/uploading-to-s3-with-angularjs/
app.directive("file", function () {
    return {
        restrict: "AE",
        scope: {
            file: "@"
        },
        link: function (scope, el, attrs) {
            el.bind("change", function (event) {
                var files = event.target.files;
                var file = files[0];
                scope.file = file;
                scope.$parent.file = file;
                scope.$apply();
            });
        }
    };
});

function sessionService() {
    this.isAuthenticated = function () {
        if (this.getUserId() == "" || this.getUserRole() == "") {
            return false;
        } else {
            return true;
        }
    }

    this.isAuthorized = function (rolesPermitidos) {
        //Roles validos: CONSUMIDOR, PRODUCTOR, ADMINISTRADOR
        if (this.isAuthenticated()) {
            if (angular.isArray(rolesPermitidos)) {
                if (rolesPermitidos.indexOf(this.getUserRole()) !== -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    this.getUserId = function () {
        var local = localStorage["organicos_data"];
        if (!this.isJsonString(local)) {
            return "";
        } else {
            var json = JSON.parse(local);
            return json.id;
        }

    }

    this.getUserRole = function () {
        var local = localStorage["organicos_data"];
        if (!this.isJsonString(local)) {
            return "";
        } else {
            var json = JSON.parse(local);
            return json.rol;
        }
    }
    /*
     this.isUndefinedOrNull = function(val) {
     return !angular.isDefined(val) || val === null;
     }*/

    this.isJsonString = function (str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}

sessionService();

app.service("sessionService", sessionService);