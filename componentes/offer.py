#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import json
from pprint import pprint

import createOffer
from django.core import serializers
from django.http import HttpResponse
from .models import Oferta, ListaValores, Productor, ItemOferta


# Función encargada de informar si la ventana de tiempo para
# confimar oferta se encuentra abierta. Para ello se debe estar
# en día domingo entre 1pm y 5pm.
# Retorna True en caso de que la ventana esté abierta, False de
# lo contrario.
def queryOfferWindow(request):
    if request.method == 'GET':
        current_date = datetime.datetime.now()
        current_weekday = current_date.date().weekday()
        current_hour = current_date.hour
        print('Consultando ventana de confirmacion')
        print ('Dia de la semana actual: ' + str(current_weekday))
        print ('Hora actual: ' + str(current_hour))
        db_weekday = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='DiaVentanaConfirmacion')[0].valor))
        db_start_hour = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='HoraInicioVentanaConfirmacion')[0].valor))
        db_end_hour = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='HoraFinVentanaConfirmacion')[0].valor))
        if current_weekday == db_weekday and db_start_hour <= current_hour <= db_end_hour:
            return HttpResponse(json.dumps(True), status=200)
        else:
            return HttpResponse(json.dumps(False), status=200)


# Función encargada de retornar el día, hora de inicio y hora de fin
# comprendidos por el plazo de confirmación de oferta.
def queryOfferWindowValue(request):
    try:
        if request.method == 'GET':
            item_temp = {}
            item_temp['dia'] = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='DiaVentanaConfirmacion')[0].valor))
            item_temp['horaInicio'] = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='HoraInicioVentanaConfirmacion')[0].valor))
            item_temp['horaFin'] = int(float(ListaValores.objects.filter(tipo='Oferta', codigo='HoraFinVentanaConfirmacion')[0].valor))
            return HttpResponse(json.dumps(item_temp), status=200)

        if request.method == 'POST':
            jsonWindowValue = json.loads(request.body)
            item_temp = {}
            dia = ListaValores.objects.filter(tipo='Oferta', codigo='DiaVentanaConfirmacion')[0]
            dia.valor = jsonWindowValue['dia']
            dia.save()

            horaInicio = ListaValores.objects.filter(tipo='Oferta', codigo='HoraInicioVentanaConfirmacion')[0]
            horaInicio.valor = jsonWindowValue['horaInicio']
            horaInicio.save()

            horaFin = ListaValores.objects.filter(tipo='Oferta', codigo='HoraFinVentanaConfirmacion')[0]
            horaFin.valor = jsonWindowValue['horaFin']
            horaFin.save()

            item_temp['dia'] = int(float(dia.valor))
            item_temp['horaInicio'] = int(float(horaInicio.valor))
            item_temp['horaFin'] = int(float(horaFin.valor))

            return HttpResponse(json.dumps(item_temp), status=200)

    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de retornar una oferta dado el ID del usuario.
def queryOfferById(request, id):
    if request.method == 'GET':
        producer = Productor.objects.all().get(idUsuario=id)
        offers = Oferta.objects.all().filter(idProductor=producer.id)

        if offers.count() > 1:
            offer = offers.latest('fechaRegistro')
        else:
            for o in offers:
                offer = o
        return HttpResponse(serializers.serialize('json', [offer]))


# Función encargada de confirmar o cancelar una oferta dada la información
# suministrada por el usuario (1: confirmada, 0: rechazada, -1: indefinido)
def confirmOffer(request, id, value):
    print("Confirmando oferta")
    try:
        if request.method == 'GET':
            producer = Productor.objects.all().get(idUsuario=id)
            offers = Oferta.objects.all().filter(idProductor=producer.id)

            if offers.count() >= 1:
                offer = offers.latest('fechaRegistro')
                currentDate = datetime.datetime.now().date()
                print('Oferta encontrada')
                print(offer.id)
                startDate = offer.fechaInicio - datetime.timedelta(days=7)
                endDate = offer.fechaFin - datetime.timedelta(days=7)

                if (endDate >= currentDate >= startDate):
                    print value
                    offer.estado = value
                    offer.save()
                    return HttpResponse(serializers.serialize('json', [offer]))
                else:
                    raise StandardError('No hay una oferta disponible para confirmar')
            else:
                raise StandardError('No hay una oferta disponible para confirmar')
    except Exception as e:
        pprint(e.args)
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite obtener todas las ofertas realizadas
def queryAllOffers(request):
    try:
        return HttpResponse(serializers.serialize('json', Oferta.objects.all()), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite obtener todas las ofertas realizadas por un productor
# JdRunza Se modifica para que cree la oferta
def queryOffersByProducer(request, value):
    if request.method == 'GET':
        try:
            return HttpResponse(serializers.serialize('json', createOffer.createOffersByProducer(value)))
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de retornar la oferta del periodo actual
# dado el id del usuario. De exitir retonra un json con la
# oferta, de lo contrario informa la situación con código 400.
def queryCurrentOffer(request, id):
    try:
        if request.method == 'GET':
            producer = Productor.objects.all().get(idUsuario=id)
            offers = Oferta.objects.all().filter(idProductor=producer.id)

            if offers.count() > 1:
                offer = offers.latest('fechaRegistro')
            else:
                for o in offers:
                    offer = o

            currentDate = datetime.datetime.now().date()
            startDate = offer.fechaInicio - datetime.timedelta(days=7)
            endDate = offer.fechaFin - datetime.timedelta(days=7)

            print currentDate
            print startDate
            print endDate

            if endDate >= currentDate >= startDate:
                print "entra"
                return HttpResponse(serializers.serialize('json', [offer]))
            else:
                return HttpResponse("No hay oferta para el periodo actual.", content_type="application/json",
                                    status=400)

    except Exception as e:
        pprint(e.message)
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def getResumenOrdenesOferta(request, id):
    now = datetime.datetime.now()
    try:
        ofertaActual = Oferta.objects.filter(idProductor=Productor.objects.get(idUsuario=id)).filter(fechaFin__gte=now).filter(fechaInicio__lte=now)
        item_list = []
        if ofertaActual is not None and len(ofertaActual) > 0 and int(ofertaActual.first().estado) == 1:
            total_disponible = 0
            total_solicitado = 0
            total_ofertado = 0
            for item in ItemOferta.objects.filter(idOferta=ofertaActual.first().id):
                subtotal_ofertado = int(item.cantidadOrigen) * int(item.precio)
                subtotal_disponible = int(item.cantidadActual) * int(item.precio)
                cantidad_solicitada = int(item.cantidadOrigen) - int(item.cantidadActual)
                subtotal_solicitado = cantidad_solicitada * int(item.precio)
                item_list.append(createSubItem(cantidad_solicitada, item, subtotal_disponible, subtotal_ofertado, subtotal_solicitado))
                total_disponible = total_disponible + subtotal_disponible
                total_ofertado = total_ofertado + subtotal_ofertado
                total_solicitado = total_solicitado + subtotal_solicitado
            item_list.append(total_ofertado)
            item_list.append(total_disponible)
            item_list.append(total_solicitado)
        return HttpResponse(json.dumps(item_list), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite crear los elementos de detalle del resumen de la oferta
def createSubItem(cantidad_solicitada, item, subtotal_disponible, subtotal_ofertado, subtotal_solicitado):
    item_temp = {}
    item_temp['producto'] = item.idProducto.nombre
    item_temp['unidad'] = item.idProducto.unidad
    item_temp['precio_unidad'] = item.precio
    item_temp['cantidad_ofertada'] = item.cantidadOrigen
    item_temp['subtotal_ofertado'] = subtotal_ofertado
    item_temp['cantidad_disponible'] = item.cantidadActual
    item_temp['subtotal_disponible'] = subtotal_disponible
    item_temp['cantidad_solicitada'] = cantidad_solicitada
    item_temp['subtotal_solicitado'] = subtotal_solicitado
    return item_temp
