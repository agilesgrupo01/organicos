#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Jorge on 08/03/2016.

import datetime
import json

from django.core import serializers
from django.http import HttpResponse
from .models import Oferta, Producto, ItemOferta


# Permite obtener los elementos de una ofertas específica.
# También se encarga de eliminar un ítem de oferta dado
# el ID de la misma.
def queryOfferItemsByOffer(request, value):
    try:
        if request.method == 'DELETE':
            offerItem = ItemOferta.objects.get(id=value)
            offerItem.delete()
            return HttpResponse(content_type="application/json", status=200)
        else:
            return HttpResponse(serializers.serialize('json', ItemOferta.objects.filter(idOferta=value)),
                                content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def reportOffer(request):
    try:
        print ("entro" + " fin")
        jsonSearchParameter = json.loads(request.body)
        fechaInicio = jsonSearchParameter['fechaInicio']
        print ("" + fechaInicio)
        fechaFin = jsonSearchParameter['fechaFin']
        print ("" + fechaFin)
        idProducto = jsonSearchParameter['idProducto']
        actualDate = datetime.datetime.now()
        print ("Prod " + str(idProducto) + " " + str(actualDate))
        if (fechaInicio != "" and fechaFin != "" and idProducto != 0):
            print ("entro 1")
            offerItems = ItemOferta.objects.filter(idProducto=idProducto, idOferta__fechaInicio__gte=fechaInicio,
                                                   idOferta__fechaFin__lte=fechaFin)
        elif (fechaInicio != "" and fechaFin != "" and idProducto == 0):
            print ("entro 2")
            offerItems = ItemOferta.objects.filter(idOferta__fechaInicio__gte=fechaInicio,
                                                   idOferta__fechaFin__lte=fechaFin)
        elif (idProducto != 0):
            print ("entro 3")
            offerItems = ItemOferta.objects.filter(idProducto=idProducto)
        else:
            offerItems = ItemOferta.objects.all()
        response_data = generateResponseOffer(offerItems)
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de crear un ítem de oferta.
def createOfferItem(request):
    try:
        if request.method == 'POST':
            jsonOfferItem = json.loads(request.body)
            idOferta = Oferta.objects.get(id=jsonOfferItem['idOferta'])
            idProducto = Producto.objects.get(id=jsonOfferItem['idProducto'])
            cantidadOrigen = int(jsonOfferItem['cantidadOrigen'])
            cantidadActual = int(jsonOfferItem['cantidadOrigen'])
            precio = float(jsonOfferItem['precio'])

            itemOferta = ItemOferta.objects.filter(idOferta=idOferta, idProducto=idProducto)

            if cantidadOrigen == 0 or precio == 0:
                return HttpResponse("La cantidad ingresada y el precio del producto debe ser mayor a 0.", status=400)

            if itemOferta.count() == 0:
                newOfferItem = ItemOferta(idOferta=idOferta, idProducto=idProducto, cantidadOrigen=cantidadOrigen,
                                          cantidadActual=cantidadActual, precio=precio)
                newOfferItem.save()
                return HttpResponse(serializers.serialize('json', [newOfferItem]), content_type="application/json")
            else:
                return HttpResponse("El producto ya fue registrado en la oferta.", status=400)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite consultar el producto de la oferta por id
def queryProductFromOfferById(request, value):
    if request.method == 'GET':
        try:
            return HttpResponse(json.dumps(generateOfferItem(value)), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
    elif request.method == 'POST':
        jsonOfferItem = json.loads(request.body)
        try:
            item = ItemOferta.objects.get(id=jsonOfferItem['id'])
            item.cantidadOrigen = jsonOfferItem['cantidadOrigen']
            item.cantidadActual = jsonOfferItem['cantidadOrigen']
            item.precio = jsonOfferItem['precio']
            item.save()
            return HttpResponse(serializers.serialize('json', [item]), content_type="application/json")
        except Exception as e:
            print e
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Genera un arreglo con los componentes a mostrar en la vista del item oferta
def generateOfferItem(value):
    response_data = []
    response_data_temp = {}
    item = ItemOferta.objects.get(id=value)
    response_data_temp['id'] = item.id
    response_data_temp['cantidadOrigen'] = item.cantidadOrigen
    response_data_temp['precio'] = item.precio
    response_data_temp['nombre'] = item.idProducto.nombre
    response_data.append(response_data_temp)
    return response_data


# Genera un arreglo con los componentes a mostrar en la vista del item oferta
def generateResponseOffer(offerItems):
    try:
        response_data = []
        response_data_temp = {}
        for item in offerItems:
            response_data_temp['id'] = item.id
            response_data_temp['cantidadOrigen'] = item.cantidadOrigen
            response_data_temp['cantidadActual'] = item.cantidadActual
            response_data_temp['precio'] = item.precio
            response_data_temp['fechaRegistro'] = str(item.fechaRegistro.strftime('%m/%d/%Y'))
            response_data_temp['unidad'] = item.idProducto.unidad
            response_data_temp['nombre'] = item.idProducto.nombre
            response_data_temp['idOferta'] = item.idOferta.id
            response_data_temp['fechaInicioOferta'] = str(item.idOferta.fechaInicio.strftime('%Y/%m/%d'))
            response_data_temp['fechaFinOferta'] = str(item.idOferta.fechaFin.strftime('%Y/%m/%d'))
            response_data_temp['nombreProductor'] = item.idOferta.idProductor.nombre
            response_data.append(response_data_temp)
            response_data_temp = {}
        return response_data
    except Exception as e:
        print e
