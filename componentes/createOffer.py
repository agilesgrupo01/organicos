import datetime
from datetime import timedelta

from .models import Oferta, ListaValores, Productor


# JdRunza Se crea para que cree la oferta

def createOffersByProducer(value):
    try:
        productor = Productor.objects.all().filter(idUsuario=value)
        actualDate = datetime.datetime.now()
        # Semana actual del anio (comienza los lunes)
        # 0 = Sunday
        semana = int(datetime.date(actualDate.year, actualDate.month, actualDate.day).strftime("%W"))
        # El dia en la ventana de confirmacion esta almacenado con la codificacion: 0 = Lunes, 6 = Domingo
        # En la funcion strptime %W se maneja con la codificacion: 0= Domingo , 6 = Sabado
        diaVentanaConfirmacion = int(
            (float(ListaValores.objects.filter(tipo='Oferta', codigo='DiaVentanaConfirmacion')[0].valor)))+1
        # Se cambia el dia a la codificacion apropiada para el tipo strptime y se le suma un dia
        if diaVentanaConfirmacion > 6:
            diaVentanaConfirmacion = diaVentanaConfirmacion- 7
        db_weekday = str(diaVentanaConfirmacion)
        d = str(actualDate.year) + "-W" + str(semana + 1)
        # Se establece como fecha inicial el dia de confirmacion de la semana actual del anio actual
        ini = datetime.datetime.strptime(str(d) + '-' + db_weekday, "%Y-W%W-%w")
        fin = ini + timedelta(days=6)
        ini = ini.date()
        fin = fin.date()
        offers = Oferta.objects.all().filter(idProductor=productor[0].id, fechaInicio=ini, fechaFin=fin)
        if (offers.count() < 1):
            offer = Oferta(
                estado=-1,
                idProductor=productor[0],
                precioTotal=0,
                fechaInicio=ini,
                fechaFin=fin
            )
            offer.save()
        return Oferta.objects.filter(idProductor=productor[0].id)
    except Exception as e:
        print (e)
        return e
