#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Jorge Runza on 08/03/2016.
import copy
import datetime
import json
from pprint import pprint

from componentes.product import getUUID

from django.core import serializers
from django.http import HttpResponse
from .models import Pedido, ItemPedido, Consumidor, Oferta, ItemOferta, Franja, ItemCanasta, Usuario


def queryAllOrders(request, value):
    if request.method == 'GET':
        try:
            cons = Consumidor.objects.all().filter(id_usuario=value)
            id = cons[0].id,
            actualDate = datetime.datetime.now()
            semana = datetime.date(actualDate.year, actualDate.month, actualDate.day).strftime("%W")
            orders = Pedido.objects.all().filter(idConsumidor=id).order_by('fechaRegistro').reverse()
            if (orders.count() < 1):
                order = Pedido(
                    estado=0,
                    idConsumidor=cons[0],
                    precioTotal=0
                )
                order.save()
                item = order
            else:
                item = orders[0]
                consumidor = item.idConsumidor
                fecha = item.fechaRegistro
                semUlt = datetime.date(fecha.year, fecha.month, fecha.day).strftime("%W")
                if (str(semana) != str(semUlt)):
                    print (str(semana) + " " + str(semUlt))
                    order = Pedido(
                        estado=0,
                        idConsumidor=consumidor,
                        precioTotal=0
                    )
                    order.save()
                    item = order
            response_data = [item]
            pprint(response_data)
            return HttpResponse(serializers.serialize('json', response_data))
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def queryDatilOrder(request, value):
    if request.method == 'GET':
        try:
            response_data = generateResponse(value)
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def generateResponse(value):
    response_data = []
    response_data_temp = {}
    orderItems = ItemPedido.objects.all().filter(idPedido=value)

    for item in orderItems:
        response_data_temp['id'] = item.id
        response_data_temp['nombre'] = item.idProducto.nombre
        response_data_temp['foto'] = item.idProducto.foto
        response_data_temp['precioUnidad'] = item.idProducto.precio
        response_data_temp['precioTotal'] = item.precio
        response_data_temp['cantidad'] = item.cantidad
        response_data_temp['fechaRegistro'] = str(item.fechaRegistro)
        response_data_temp['unidad'] = item.idProducto.unidad
        response_data_temp['idProducto'] = item.idProducto.id
        response_data.append(response_data_temp)
        response_data_temp = {}

    return response_data


# Función encargada de efectuar el pago del pedido. Se modifica el
# estado de 0 (pendiente) a 1 (comprado). Así mismo, registra la disminución
# del inventario de acuerdo a la regla de negocio (primero se descuentan
# unidades de los productores más económicos).
def buyOrder(request, value, idfranja):
    if request.method == 'GET':
        try:
            actualDate = datetime.datetime.now()
            itemsOferta = []
            itemsIterate = []
            order = Pedido.objects.get(id=value)
            if (order.estado == 1):
                return HttpResponse("Tu pedido de esta semana ya fue completado, te esperamos la siguiente semana.", content_type="application/json", status=400)
            else:
                itemsIterate = agregarProductosCanasta(ItemPedido.objects.all().filter(idPedido=value))
                print (itemsIterate)
                for item in itemsIterate:
                    cant = item.cantidad
                    for oferta in Oferta.objects.all().filter(estado=1, fechaInicio__lte=actualDate, fechaFin__gte=actualDate).order_by('fechaRegistro'):
                        if (cant > 0):
                            for itemOf in ItemOferta.objects.all().filter(idOferta=oferta.id, idProducto=item.idProducto, cantidadActual__gt=0):
                                canItm = itemOf.cantidadActual - cant
                                if (canItm >= 0):
                                    itemOf.cantidadActual = canItm
                                    cant = 0
                                else:
                                    itemOf.cantidadActual = 0
                                    cant = cant - itemOf.cantidadActual
                                itemsOferta.append(itemOf)
                    if (cant > 0):
                        return HttpErrorHandler("No hay la suficiente cantidad de productos para el item " + str(item.id) + " " + item.idProducto.nombre + " en las ofertas de esta semana, por favor modifique la cantidad.")
                saveItemsOferta(itemsOferta)
                franja = Franja.objects.get(id=int(idfranja))
                if franja is None:
                    return HttpErrorHandler("No existe la franja especificada ")
                else:
                    return HttpResponse(serializers.serialize('json', [saveOrder(order, franja)]))
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def agregarProductosCanasta(items):
    try:
        itemsCopy = []
        for item in items:
            print item.idProducto.idCategoria.nombre
            if (item.idProducto.idCategoria.nombre == "Canasta"):
                for itemCanasta in ItemCanasta.objects.all().filter(idCanasta=item.idProducto):
                    itemTemp = {}
                    itemTemp = copy.copy(item)
                    itemTemp.idProducto = itemCanasta.idProducto
                    itemTemp.cantidad = (itemCanasta.cantidad) * item.cantidad
                    itemsCopy.append(itemTemp)
            else:
                itemsCopy.append(item)
        return itemsCopy
    except Exception as e:
        print e
        return e


# Permite registrar los items de una oferta
def saveItemsOferta(itemsOferta):
    for it in itemsOferta:
        it.save()


# Permite manejar los errores que se retornan del servicio
def HttpErrorHandler(errorMsg):
    error = {}
    error['desc'] = errorMsg
    return HttpResponse(json.dumps([error]), content_type="application/json", status=400)


# Pemite almacenar una orden con la franja a la que pertenece
def saveOrder(order, franja):
    order.idFranja = franja
    order.estado = 1
    order.save()
    return order


# Devuelve las franjas que tiene disponible el usuario para solicitar el pedido
def getFranjasDisponibles(request):
    now = datetime.datetime.now()
    limitTime = now.time()
    limitTime = limitTime.replace(12, 0, 0, 0, limitTime.tzinfo)
    # Verifica si ya se paso del medio dia
    searchTime = now.replace(now.year, now.month, now.day, 0, 0, 0, 0, now.tzinfo)
    if now.time() < limitTime:
        searchTime = searchTime + datetime.timedelta(days=1)
    else:
        searchTime = searchTime + datetime.timedelta(days=2)

    franjas = Franja.objects.filter(fechaInicio__gte=searchTime).filter(tipo='PEDIDO')
    diasFranjas = []
    dias = {}

    for franja in franjas:
        dia = getDayName(int(franja.fechaInicio.strftime("%w"))) + ' ' + franja.fechaInicio.strftime("%Y-%m-%d")
        if not (dia in diasFranjas):
            diasFranjas.append(dia)
    dias['diasDisponibles'] = diasFranjas

    try:
        franjasVista = []
        franjasVista.append(dias)
        for franja in franjas:
            franjasVista.append(franjaToDAO(franja))
        return HttpResponse(json.dumps(franjasVista), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def getDayName(value):
    dayName = 'Domingo'
    if value == 0:
        dayName = 'Domingo'
    elif value == 1:
        dayName = 'Lunes'
    elif value == 2:
        dayName = 'Martes'
    elif value == 3:
        dayName = 'Miercoles'
    elif value == 4:
        dayName = 'Jueves'
    elif value == 5:
        dayName = 'Viernes'
    elif value == 6:
        dayName = 'Sabado'

    return dayName


def franjaToDAO(franja):
    franja_temp = {}
    franja_temp['id'] = franja.id
    franja_temp['nombreDia'] = getDayName(int(franja.fechaInicio.strftime("%w")))
    franja_temp['dia'] = getDayName(int(franja.fechaInicio.strftime("%w"))) + ' ' + franja.fechaInicio.strftime(
        "%Y-%m-%d")
    franja_temp['franja'] = franja.fechaInicio.strftime('%H:%M') + ' - ' + franja.fechaFin.strftime('%H:%M')
    return franja_temp


# Devuelve el DAO de la franja correspondiente al id que se pasa como parámetro
def getDAOFranja(request, id_franja):
    pprint("Buscando franja" + id_franja)
    try:
        franjaVista = []
        franja = franjaToDAO(Franja.objects.get(id=id_franja))
        franjaVista.append(franja)
        return HttpResponse(json.dumps(franjaVista), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Devuelve las franjas que tiene disponible el usuario para solicitar el pedido (Para la  aplicacion Movil)
def getFranjasDisponiblesMOVIL(request):
    now = datetime.datetime.now()
    limitTime = now.time()
    limitTime = limitTime.replace(12, 0, 0, 0, limitTime.tzinfo)
    # Verifica si ya se paso del medio dia
    searchTime = now.replace(now.year, now.month, now.day, 0, 0, 0, 0, now.tzinfo)
    if now.time() < limitTime:
        searchTime = searchTime + datetime.timedelta(days=1)
    else:
        searchTime = searchTime + datetime.timedelta(days=2)

    franjas = Franja.objects.filter(fechaInicio__gte=searchTime).filter(tipo='PEDIDO')

    try:
        franjasVista = []
        for franja in franjas:
            franjasVista.append(franjaToJSONMovil(franja))
        return HttpResponse(json.dumps(franjasVista), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Devuelve al formato JSON que lee el movil
def franjaToJSONMovil(franja):
    franja_json = {}
    franja_json['id'] = franja.id
    franja_json['description'] = (franja.fechaInicio - datetime.timedelta(hours=5)).strftime("%Y-%m-%d %H:%M")
    return franja_json


# Función encargada de efectuar el pago del pedido como servicio REST para la apliacion movil. Se modifica el
# estado de 0 (pendiente) a 1 (comprado). Así mismo, registra la disminución
# del inventario de acuerdo a la regla de negocio (primero se descuentan
# unidades de los productores más económicos).
def confirmarCompraJSON(request):
    if request.method == 'POST':
        try:
            actualDate = datetime.datetime.now()
            jsonOrderItem = json.loads(request.body.decode("utf-8"))
            if validarUsuario(jsonOrderItem['username'], jsonOrderItem['password']):
                itemsOferta = []
                order = consultarPedidoConsumidor(jsonOrderItem['username'])
                if (order.estado == 1):
                    return HttpResponse(json.dumps({"status": "ERROR", "message": "El pedido ya fue confirmado"}), content_type="application/json")
                else:
                    for item in ItemPedido.objects.all().filter(idPedido=order):
                        cant = item.cantidad
                        for oferta in Oferta.objects.all().filter(estado=1, fechaInicio__lte=actualDate, fechaFin__gte=actualDate).order_by('fechaRegistro'):
                            if (cant > 0):
                                for itemOf in ItemOferta.objects.all().filter(idOferta=oferta.id, idProducto=item.idProducto, cantidadActual__gt=0):
                                    canItm = itemOf.cantidadActual - cant
                                    if (canItm >= 0):
                                        itemOf.cantidadActual = canItm
                                        cant = 0
                                    else:
                                        itemOf.cantidadActual = 0
                                        cant = cant - itemOf.cantidadActual
                                    itemsOferta.append(itemOf)
                        if (cant < 0):
                            return HttpResponse(
                                json.dumps({"status": "ERROR", "message": "No hay la suficiente cantidad de productos para el item " + str(item.id) + " " + item.idProducto.nombre + " en las ofertas de esta semana, por favor modifique la cantidad."}),
                                content_type="application/json")
                    saveItemsOferta(itemsOferta)
                    franja = getUltimaFranjaDisponible()
                    saveOrder(order, franja)
                    return HttpResponse(json.dumps({"status": "OK", "message": "Confirmada la compra"}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"status": "ERROR", "message": "Usuario o contraseña no válido"}), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Devuelve el pedido actual del cliente, si no existe lanza un error
def consultarPedidoConsumidor(consumidor):
    orders = Pedido.objects.all().filter(idConsumidor__id_usuario__correoElectronico=consumidor).order_by('fechaRegistro').reverse()
    pedido_actual = None
    if (orders.count() < 1):
        raise LookupError("El usuario no tiene pedidos")
    else:
        pedido_actual = orders[0]
        fecha = pedido_actual.fechaRegistro
        semUlt = datetime.date(fecha.year, fecha.month, fecha.day).strftime("%W")
        actualDate = datetime.datetime.now()
        semanaActual = datetime.date(actualDate.year, actualDate.month, actualDate.day).strftime("%W")
        if (str(semanaActual) <> str(semUlt)):
            raise LookupError("El usuario no tiene pedidos")

    return pedido_actual


# Valida si la contraseña corresponde a la del usuario
def validarUsuario(username, password):
    usuario = Usuario.objects.get(correoElectronico=username)
    if (usuario.contrasenia == password):
        return True
    else:
        return False


# Devuelve la ultima franja que hay disponible para solicitar el pedido
# Si no existe lanza un error
def getUltimaFranjaDisponible():
    now = datetime.datetime.now()
    limitTime = now.time()
    limitTime = limitTime.replace(12, 0, 0, 0, limitTime.tzinfo)
    # Verifica si ya se paso del medio dia
    searchTime = now.replace(now.year, now.month, now.day, 0, 0, 0, 0, now.tzinfo)
    if now.time() < limitTime:
        searchTime = searchTime + datetime.timedelta(days=1)
    else:
        searchTime = searchTime + datetime.timedelta(days=2)

    franja = Franja.objects.filter(fechaInicio__gte=searchTime).filter(tipo='PEDIDO').latest("fechaFin")

    if franja:
        return franja
    else:
        raise LookupError("No hay franjas disponibles para solicitar el pedido")


def generateResponseConsumidor(consumidor):
    response_data = []
    response_data_temp = {}
    orderItems = ItemPedido.objects.all().filter(idPedido=consultarPedidoConfirmadoConsumidor(consumidor))

    if orderItems.__sizeof__() < 1:
        raise LookupError("No hay solicitudes en el pedido")
    else:
        for item in orderItems:
            response_data_temp['id'] = getUUID(item.id)
            response_data_temp['name'] = item.idProducto.nombre
            response_data_temp['quantity'] = item.cantidad
            response_data_temp['price'] = item.idProducto.precio
            response_data_temp['unit'] = item.idProducto.unidad
            response_data_temp['total'] = item.precio
            response_data.append(response_data_temp)
            response_data_temp = {}

    return response_data


# Devuelve el pedido actual del cliente si ha sido confirmado, si no existe o no ha sido confirmado lanza un error
def consultarPedidoConfirmadoConsumidor(consumidor):
    orders = Pedido.objects.all().filter(idConsumidor=consumidor).filter(estado=1).order_by('fechaRegistro').reverse()
    pedido_actual = None
    if orders.count() < 1:
        raise LookupError("El usuario no tiene confirmado un pedido para esta semana")
    else:
        pedido_actual = orders[0]
        fecha = pedido_actual.fechaRegistro
        semUlt = datetime.date(fecha.year, fecha.month, fecha.day).strftime("%W")
        actualDate = datetime.datetime.now()
        semanaActual = datetime.date(actualDate.year, actualDate.month, actualDate.day).strftime("%W")
        if (str(semanaActual) <> str(semUlt)):
            raise LookupError("El usuario no tiene confirmado un pedido en la fecha actual")
    return pedido_actual


def getListaProductosComprados(request):
    jsonOrderItem = json.loads(request.body)
    consumidor = Consumidor.objects.filter(id_usuario__correoElectronico=jsonOrderItem['username'])

    try:
        if consumidor:
            return HttpResponse(json.dumps(generateResponseConsumidor(consumidor)), content_type="application/json")
        else:
            raise LookupError("No existe el usuario que se paso como parametro")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
