# -*- coding: utf-8 -*-

import datetime
import json

from componentes.models import Usuario, Franja, Categoria, Producto, Oferta, Productor, ItemOferta, Consumidor, Pedido, ItemPedido

from django.test import Client, TestCase


class TestServiciosREST(TestCase):
    def test_login_usuario(self):
        insertarConsumidorPrueba()

        c = Client()
        response = c.post('/login/', json.dumps({'username': 'usu@mail.com', 'password': '1234'}),
                          content_type="application/json")

        imprimirAConsola('test_login_usuario', response.content)

        json_data = json.loads(response.content)
        self.assertEqual(json_data['status'], "OK", "El usuario no se pudo registrar en la aplicacion")

    # Se debe realizar como mínimo un sábado antes del medio día
    def test_ver_horarios_envio(self):
        # Se crea una franja para validar el servicio
        searchTime = consultarFechaMasCercanaFranja()
        franja = Franja(tipo='PEDIDO', semana=0, fechaInicio=searchTime, fechaFin=searchTime + datetime.timedelta(hours=2))
        franja.save()
        descripcion = (franja.fechaInicio).strftime("%Y-%m-%d %H:%M")
        imprimirAConsola('valor des', descripcion)

        c = Client()
        response = c.get('/order/schedule/')

        imprimirAConsola('test_ver_horarios_envio', response.content)

        json_data = json.loads(response.content)[0]
        self.assertEqual(json_data['description'], descripcion, "No se pudo consultar las franjas disponibles para pedido")

    def test_listar_productos_ofertados_de_la_semana(self):
        insertarOfertasPrueba(getNow())

        c = Client()
        response = c.get('/products/')

        imprimirAConsola('test_listar_productos_ofertados_de_la_semana xxx', response.content)

        json_data = json.loads(response.content)[0]
        self.assertEqual(json_data['name'], 'Papa', "El nombre del producto consultado no coincide con el de la BD")
        self.assertEqual(json_data['price'], 1000, "El precio del producto consultado no coincide con el de la BD")
        self.assertEqual(json_data['stock'], 50, "El stock del producto consultado no coincide con el de la BD")

    # Se debe realizar como mínimo un sábado antes del medio día
    def test_agregar_producto_a_pedido_semanal(self):
        now = getNow()
        insertarConsumidorPrueba()
        usuario = insertarConsumidorPrueba()
        insertarPedidoPrueba(usuario,now,-1)
        producto = insertarProducto()

        c = Client()
        response = c.put('/cart/add/', json.dumps({'username': 'usu@mail.com', 'id_product': producto.id, "quantity": 10}),
                         content_type="application/json")

        imprimirAConsola('TEST_AGREGAR_PRODUCTO_A_PEDIDO', response.content)

        json_data = json.loads(response.content)
        self.assertEqual(json_data['status'], "OK", "El producto no se añadió al pedido")

    def test_confirmar_pedido_semanal(self):
        now = getNow()
        usuario = insertarConsumidorPrueba()
        insertarOfertasPrueba(now)
        producto = insertarProducto()
        pedido = insertarPedidoPrueba(usuario, now, -1)
        insertarItemEnPedido(pedido, producto)

        c = Client()
        response = c.post('/cart/checkout/', json.dumps({"username": "usu@mail.com", "password": "1234", "confirmation": True}),
                          content_type="application/json")

        imprimirAConsola('TEST_CONFIRMAR_PEDIDO', response.content)

        json_data = json.loads(response.content)
        self.assertEqual(json_data['status'], "OK", "El pedido no se pudo confirmar")

    def test_ver_lista_productos_comprados(self):
        now = getNow()
        usuario = insertarConsumidorPrueba()
        insertarOfertasPrueba(now)
        producto = insertarProducto()
        pedido = insertarPedidoPrueba(usuario, now, 1)
        insertarItemEnPedido(pedido, producto)

        c = Client()
        response = c.post('/order/', json.dumps({"username": "usu@mail.com"}),
                          content_type="application/json")

        imprimirAConsola('TEST_LISTAR_PRODUCTOS_COMPRADOS', response.content)

        json_data = json.loads(response.content)[0]
        self.assertEqual(json_data['name'], 'Papa', "El nombre del producto consultado no coincide con el de la BD")
        self.assertEqual(json_data['quantity'], 10, "La cantidad del producto consultado no coincide con el de la BD")
        self.assertEqual(json_data['total'], 10000, "El precio total del producto consultado no coincide con el de la BD")


# Utilidades

def consultarFechaMasCercanaFranja():
    now = getNow()
    limitTime = now.time()
    limitTime = limitTime.replace(12, 0, 0, 0, limitTime.tzinfo)
    # Verifica si ya se paso del medio dia
    searchTime = now.replace(now.year, now.month, now.day, 0, 0, 0, 0, now.tzinfo)
    if now.time() < limitTime:
        searchTime = searchTime + datetime.timedelta(days=1)
    else:
        searchTime = searchTime + datetime.timedelta(days=2)

    return searchTime


def imprimirAConsola(label, resultado):
    print(" ")
    print("--------------- Respuesta " + label + ": ----------------------")
    print(resultado)
    print("--------------- FIN Respuesta " + label + ": ----------------------")


def getNow():
    return datetime.datetime.now()


def semanaAnterior(fecha):
    return fecha - datetime.timedelta(days=7)


def primerDiaSemanaActual(now):
    return now.replace(year=now.year, month=now.month, day=now.day - now.weekday(),
                       hour=0, minute=0, second=0, microsecond=0)


def ultimoDiaSemanaActual(now):
    return now.replace(year=now.year, month=now.month, day=now.day + (6 - now.weekday()),
                       hour=23, minute=59, second=59, microsecond=999999)


def insertarConsumidorPrueba():
    usuario = Usuario(correoElectronico='usu@mail.com', contrasenia='1234', tipo='Consumidor', fechaRegistro=datetime.datetime.now())
    usuario.save()
    return usuario


def insertarOfertasPrueba(now):
    # Se crea una franja para validar el servicio

    producto = insertarProducto()

    usuProductor = Usuario(correoElectronico='prod@mail.com', contrasenia='1234', tipo='Productor', fechaRegistro=now)
    usuProductor.save()

    usuProductor2 = Usuario(correoElectronico='prod2@mail.com', contrasenia='1234', tipo='Productor', fechaRegistro=now)
    usuProductor2.save()

    productor = Productor(nombre="Productor1", telefono="3241342", celular="300123423",
                          foto='http://www.correodelorinoco.gob.ve/wp-content/uploads/2015/01/ALDEMARO-ORTEGA-4.jpg',
                          certificacion='Si', twitter='@prod', correoElectronico='prod@mail.com', direccion='calle a # 12-21',
                          idUsuario=usuProductor, fechaRegistro=now)
    productor.save()

    productor2 = Productor(nombre="Productor2", telefono="222222", celular="300222222",
                           foto='http://losandes.com.ar/files/image/15/07/image5595ce4547a343.26162672.jpg',
                           certificacion='Si', twitter='@prod2', correoElectronico='prod2@mail.com', direccion='calle 2 # 22-22',
                           idUsuario=usuProductor2, fechaRegistro=now)
    productor2.save()

    oferta = Oferta(estado=1, idProductor=productor, precioTotal=50000, fechaRegistro=semanaAnterior(now), fechaInicio=primerDiaSemanaActual(now),
                    fechaFin=ultimoDiaSemanaActual(now))
    oferta.save()

    oferta2 = Oferta(estado=1, idProductor=productor2, precioTotal=60000, fechaRegistro=semanaAnterior(now), fechaInicio=primerDiaSemanaActual(now),
                     fechaFin=ultimoDiaSemanaActual(now))
    oferta2.save()

    itemOferta1 = ItemOferta(idOferta=oferta, idProducto=producto, cantidadOrigen=50, cantidadActual=30, precio=50000, fechaRegistro=now)
    itemOferta1.save()

    itemOferta2 = ItemOferta(idOferta=oferta2, idProducto=producto, cantidadOrigen=60, cantidadActual=20, precio=60000, fechaRegistro=now)
    itemOferta2.save()


def insertarPedidoPrueba(usuario, now, estado):
    consumidor = Consumidor(nombres="Pedro Poncio", apellidos="gualteros parra", telefono=123455678, foto=None,
                            fechaRegistro=now, direccion=None, id_usuario=usuario)
    consumidor.save()
    searchTime = consultarFechaMasCercanaFranja()
    franja = Franja(tipo='PEDIDO', semana=0, fechaInicio=searchTime, fechaFin=searchTime + datetime.timedelta(hours=2))
    franja.save()
    pedido = Pedido(estado=estado, idConsumidor=consumidor, precioTotal=0, fechaRegistro=now, idFranja=franja)
    pedido.save()
    return pedido


def insertarProducto():
    now = getNow()
    categoria = Categoria(nombre='Carbohidrato', descripcion='Desc Carbohidrato', fechaRegistro=now)
    categoria.save()
    producto = Producto.objects.filter(nombre='Papa')
    if producto.count() < 1:
        producto = Producto(idCategoria=categoria, estado=1, nombre='Papa', precio=1000,
                            fechaRegistro=now, foto='http://www.experimentosfaciles.com/wp-content/uploads/2016/01/papa.jpg',
                            calificacionPromedio=0, unidad='Kg', premium=False)
        producto.save()
    else:
        producto = producto.first()
    return producto


def insertarItemEnPedido(pedido, producto):
    itempedido = ItemPedido(idPedido=pedido, idProducto=producto, cantidad=10, precio=10000, fechaRegistro=getNow())
    itempedido.save()
