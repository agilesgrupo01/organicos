#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import string
from unittest import TestCase

from selenium import webdriver


class FunctionalTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    # Prueba para acceder a la opcion de crear productor OR-15
    def test_access_to_create(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(5)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys('admin@admin.com')
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys('1234')
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_xpath("//a[text()='Productores']").click()
        self.browser.implicitly_wait(5)

        value = self.browser.find_element_by_id("addProducer").get_attribute("value")
        self.browser.implicitly_wait(5)

        self.assertIn("Agregar Productor", value)

    # Prueba para mostrar el formulario de crear productor OR-15
    def test_form_of_create(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(5)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys('admin@admin.com')
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys('1234')
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_xpath("//a[text()='Productores']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_id("addProducer").click()
        self.browser.implicitly_wait(5)

        title = self.browser.find_element_by_id("producerLabel").text
        self.browser.implicitly_wait(5)

        self.assertIn("Productor", title)

    # Prueba para diligenciar y registrar el formulario de crear productor OR-15
    def test_fill_form_and_register(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(5)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys('admin@admin.com')
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys('1234')
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_xpath("//a[text()='Productores']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_id("addProducer").click()
        self.browser.implicitly_wait(5)

        textoAleatorio = ''.join(random.sample((string.ascii_letters), 5))
        numeroAleatorio = ''.join(random.sample((string.digits), 5))

        self.browser.find_element_by_id("nombre").send_keys(textoAleatorio)
        self.browser.find_element_by_id("telefono").send_keys(numeroAleatorio)
        self.browser.find_element_by_id("celular").send_keys(numeroAleatorio)
        self.browser.find_element_by_id("direccion").send_keys(textoAleatorio)
        self.browser.find_element_by_id("certificacion").send_keys(textoAleatorio)
        self.browser.find_element_by_id("twitter").send_keys(textoAleatorio)
        self.browser.find_element_by_id("foto").send_keys("C:\test.jpg")

        self.browser.find_element_by_id("createProducer").click()
        self.browser.implicitly_wait(5)

        message = self.browser.find_element_by_id("message").text
        self.browser.implicitly_wait(5)

        self.assertIn("satisfactoriamente", message)
