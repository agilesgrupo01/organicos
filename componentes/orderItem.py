#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Jorge Runza on 17/03/2016.

import json

import datetime
from django.core import serializers
from django.http import HttpResponse

from componentes.product import getDisponibilidadProducto
from . import order
from .models import ItemPedido, Pedido, Producto, Consumidor


# Permite obtener los elementos de una orden específica.
def queryorderItemsByorder(request, value):
    try:
        orderItems = ItemPedido.objects.filter(idOrder=value)
        return HttpResponse(serializers.serialize('json', orderItems), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de eliminar un ítem de la orden dado
# el ID de la misma.
def deleteOrderItemById(request, value):
    print("Borrando item: " + str(value))
    try:
        orderItem = ItemPedido.objects.get(id=value)
        pedido = orderItem.idPedido
        pedido.precioTotal = pedido.precioTotal - orderItem.precio
        orderItem.delete()
        pedido.save()
        return HttpResponse(json.dumps(order.generateResponse(pedido)), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de crear un ítem de pedido.
def createOrderItem(request):
    print("Creando item")
    try:
        if request.method == 'POST':
            jsonOrderItem = json.loads(request.body)
            print jsonOrderItem
            idPedido = Pedido.objects.get(id=jsonOrderItem['idPedido'])
            idProducto = Producto.objects.get(id=jsonOrderItem['idProducto'])
            cantidad = int(jsonOrderItem['cantidad'])
            precio = float(jsonOrderItem['precioProducto']) * cantidad
            print jsonOrderItem['cantidad']

            itemPedido = ItemPedido.objects.filter(idPedido=idPedido, idProducto=idProducto)

            if cantidad == 0:
                return HttpResponse("La cantidad ingresada debe ser mayor a 0.", status=400)

            if itemPedido.count() == 0:
                newOrderItem = ItemPedido(idPedido=idPedido, idProducto=idProducto, cantidad=cantidad, precio=precio)
                newOrderItem.save()
                idPedido.precioTotal = idPedido.precioTotal + precio
                idPedido.save()
                return HttpResponse(serializers.serialize('json', [newOrderItem]), content_type="application/json")
            else:
                return HttpResponse("El producto ya fue registrado en el pedido.", status=400)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite consultar el producto del pedido por id
def queryProductFromOrderById(request, value):
    if request.method == 'GET':
        try:
            response_data = generateOrderItem(value)
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
    elif request.method == 'POST':
        print("Actualizando pedido de: " + str(value))
        jsonOrderItem = json.loads(request.body)
        print('Actualizando item' + str(jsonOrderItem['id']))
        try:
            item = ItemPedido.objects.get(id=jsonOrderItem['id'])
            item.cantidad = jsonOrderItem['cantidad']
            item.save()
            return HttpResponse(serializers.serialize('json', [item]), content_type="application/json")
        except Exception as e:
            print e
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Genera un arreglo con los componentes a mostrar en la vista del item del pedido
def generateOrderItem(value):
    response_data = []
    response_data_temp = {}
    item = ItemPedido.objects.get(id=value)
    print('Objeto serializado: ' + serializers.serialize('json', [item]))
    response_data_temp['id'] = item.id
    response_data_temp['cantidad'] = item.cantidad
    response_data_temp['unidad'] = item.idProducto.unidad
    response_data_temp['precio'] = item.idProducto.precio
    response_data_temp['nombreProducto'] = item.idProducto.nombre
    response_data_temp['idProducto'] = item.idProducto.id
    response_data.append(response_data_temp)
    return response_data


# Función encargada de crear un ítem de pedido para la aplicacion movil
def createOrderItemJSON(request):
    print("Creando item")
    try:
        if request.method == 'PUT':
            jsonOrderItem = json.loads(request.body)
            print jsonOrderItem
            consumidor = Consumidor.objects.get(id_usuario__correoElectronico=jsonOrderItem['username'])
            pedido_actual = consultarPedidoActual(consumidor)
            producto = Producto.objects.get(id=int(jsonOrderItem['id_product']))
            cantidad = int(jsonOrderItem['quantity'])

            itemPedidos = ItemPedido.objects.filter(idPedido=pedido_actual, idProducto=producto)
            cantidad_disponible = getDisponibilidadProducto(producto)

            if cantidad <= 0:
                return HttpResponse("La cantidad ingresada debe ser mayor a 0.", status=400)

            if cantidad>cantidad_disponible:
                return HttpResponse("La cantidad solicitada es superior a la disponible. Unidades disponibles: "+str(cantidad_disponible), status=400)

            if itemPedidos.count() == 0:
                newOrderItem = ItemPedido(idPedido=pedido_actual, idProducto=producto, cantidad=cantidad, precio=producto.precio)
                newOrderItem.save()
                pedido_actual.precioTotal = pedido_actual.precioTotal + producto.precio
                pedido_actual.save()
                return HttpResponse(json.dumps({"status":"OK","message":"Agregado al carrito"}), content_type="application/json")
            else:
                return HttpResponse("El producto ya fue registrado en el pedido.", status=400)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)

#Devuelve el pedido actual, si no existe, lo crea
def consultarPedidoActual(consumidor):
    orders = Pedido.objects.all().filter(idConsumidor=consumidor).order_by('fechaRegistro').reverse()
    if (orders.count() < 1):
        order = Pedido(
            estado=0,
            idConsumidor=consumidor,
            precioTotal=0
        )
        order.save()
        pedido_actual = order
    else:
        pedido_actual = orders[0]
        fecha = pedido_actual.fechaRegistro
        semUlt = datetime.date(fecha.year, fecha.month, fecha.day).strftime("%W")
        actualDate = datetime.datetime.now()
        semanaActual = datetime.date(actualDate.year, actualDate.month, actualDate.day).strftime("%W")
        if (str(semanaActual) <> str(semUlt)):
            order = Pedido(
                estado=0,
                idConsumidor=consumidor,
                precioTotal=0
            )
            order.save()
            pedido_actual = order

    return pedido_actual