#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core import serializers
from django.http import HttpResponse
from .models import ItemCanasta, Producto


# Función encargada de crear un ítem asociado a una canasta.
# Valida que la cantidad sea mayor a 0 y que el item no se
# haya ingresado previamente a la canasta.
def createBasketItem(request):
    try:
        if request.method == 'POST':
            jsonOfferItem = json.loads(request.body)

            print jsonOfferItem['idCanasta']
            print jsonOfferItem['idProducto']
            print jsonOfferItem['cantidad']

            idCanasta = Producto.objects.get(id=jsonOfferItem['idCanasta'])
            idProducto = Producto.objects.get(id=jsonOfferItem['idProducto'])
            cantidad = int(jsonOfferItem['cantidad'])
            precio = idProducto.precio

            itemCanasta = ItemCanasta.objects.filter(idCanasta=idCanasta, idProducto=idProducto)

            if cantidad == 0:
                return HttpResponse("La cantidad ingresada debe ser mayor a 0.", status=400)

            if itemCanasta.count() == 0:
                newBasketItem = ItemCanasta(idCanasta=idCanasta, idProducto=idProducto, cantidad=cantidad,
                                            precio=precio)
                newBasketItem.save()
                return HttpResponse(serializers.serialize('json', [newBasketItem]), content_type="application/json")
            else:
                return HttpResponse("El producto ya fue registrado en la canasta.", status=400)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de retornar los ítems asociado a una canasta.
# haya ingresado previamente a la canasta.
def queryBasketItem(request, value):
    try:

        response_data = []
        response_data_temp = {}
        itemsCanasta = ItemCanasta.objects.filter(idCanasta=value)

        for item in itemsCanasta:
            response_data_temp['id'] = item.idProducto.id
            response_data_temp['idCategoria'] = item.idProducto.idCategoria.nombre
            if (item.idProducto.estado == '1'):
                response_data_temp['estado'] = "Activo"
            elif (item.idProducto.estado == '0'):
                response_data_temp['estado'] = "Inactivo"
            else:
                response_data_temp['estado'] = item.idProducto.estado
            response_data_temp['nombre'] = item.idProducto.nombre
            response_data_temp['precio'] = item.idProducto.precio
            response_data_temp['fechaRegistro'] = str(item.fechaRegistro.strftime('%Y/%m/%d'))
            response_data_temp['foto'] = item.idProducto.foto
            response_data_temp['calificacionPromedio'] = item.idProducto.calificacionPromedio
            response_data_temp['cantidad'] = item.cantidad
            response_data_temp['unidad'] = item.idProducto.unidad
            response_data.append(response_data_temp)
            response_data_temp = {}

        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)