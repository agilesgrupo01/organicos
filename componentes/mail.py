import os
import sendgrid


class Mail:
    def __init__(self, to, cc, subject, msg):
        self.to = to
        self.cc = cc
        self.subject = subject
        self.msg = msg

    def send(self):
        sg = sendgrid.SendGridClient(os.environ.get('SENDGRID_KEY', ''))
        message = sendgrid.Mail()
        message.add_to(self.to)
        print self.to
        message.add_cc(self.cc)
        print self.cc
        message.set_subject(self.subject)
        print self.subject
        message.add_bcc("jorge.e.barrera@hotmail.com")
        message.set_html(self.msg)
        message.set_text(self.msg)
        print self.msg
        message.set_from('Organicos <organicos@organicos.com>')
        print (sg.send(message))
