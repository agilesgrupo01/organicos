from django.conf.urls import url

from . import views, product, offerItem, order, offer, orderItem, category, franja, consumer, basket, basketItem, user

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^login_view/', views.login, name='login_view'),
    url(r'^login/', views.login_rest, name='login_rest'),
    url(r'^post_login/', views.post_login, name='post_register'),

    url(r'^offers/$', offer.queryAllOffers, name='queryAllOffers'),
    url(r'^offers/producers/(?P<value>\d+)/$', offer.queryOffersByProducer, name='queryOffersByProducer'),
    url(r'^offers/window/$', offer.queryOfferWindow, name='queryOfferWindow'),
    url(r'^offers/window-value/$', offer.queryOfferWindowValue, name='queryOfferWindowValue'),
    url(r'^offers/summary/(?P<id>\d+)/$', offer.getResumenOrdenesOferta, name='getResumenOrdenesOferta'),

    url(r'^producers/$', user.queryAllProducers, name='queryAllProducers'),
    url(r'^producers/add/$', user.createProducer, name='createProducer'),
    url(r'^producers/(?P<id>\d+)/offers/(?P<value>\d+)/$', offer.confirmOffer, name='confirmOffer'),
    url(r'^producers/(?P<id>\d+)/offers/$', offer.queryOfferById, name='queryOfferById'),
    url(r'^producers/(?P<id>\d+)/offers/latest/$', offer.queryCurrentOffer, name='queryCurrentOffer'),

    url(r'^offerItems/$', offerItem.createOfferItem, name="createOfferItem"),
    url(r'^offerItems/(?P<value>\d+)/$', offerItem.queryOfferItemsByOffer, name="queryOfferItemsByOffer"),
    url(r'^offerItems/item/(?P<value>\d+)$', offerItem.queryProductFromOfferById, name="queryProductFromOfferById"),
    url(r'^offerItems/report/$', offerItem.reportOffer, name="reportOffer"),

    url(r'^product/$', product.queryAllProducts, name="queryAllProducts"),
    url(r'^products/non-basket/$', product.queryAllNonBasketProducts, name="queryAllNonBasketProducts"),
    url(r'^product/filter/$', product.queryAllProductsFilter, name="queryAllProductsFilter"),
    url(r'^product/(?P<value>\d+)/$', product.queryProductById, name="queryProductById"),
    url(r'^products/(?P<value>\d+)/$', product.editProduct, name="editProduct"),
    url(r'^products/$', product.createProduct, name="createProduct"),

    url(r'^orders/(?P<value>\d+)/$', order.queryAllOrders, name='queryAllOrders'),
    url(r'^orders/franjas/$', order.getFranjasDisponibles, name='franjasDisponibles'),
    url(r'^orders/franja/(?P<id_franja>\d+)/$', order.getDAOFranja, name='queryFranjaSeleccionada'),
    url(r'^orders/detail/(?P<value>\d+)/$', order.queryDatilOrder, name='queryDetailOrders'),
    url(r'^order/schedule/$', order.getFranjasDisponiblesMOVIL, name='queryFranjasDisponiblesMovil'),
    url(r'^order/$', order.getListaProductosComprados, name='queryListaProductosComprados'),
    url(r'^orderItems/$', orderItem.createOrderItem, name='createOrderItem'),
    url(r'^cart/add/$', orderItem.createOrderItemJSON, name='createOrderItemJson'),
    url(r'^cart/checkout/$', order.confirmarCompraJSON, name='confirmarPedidoJson'),
    url(r'^orderItems/item/(?P<value>\d+)$', orderItem.queryProductFromOrderById, name="queryProductFromOrderById"),
    url(r'^orderItem/detail/(?P<value>\d+)/$', orderItem.deleteOrderItemById, name='deleteOrderItemById'),

    url(r'^consumers/orders/buy/(?P<value>\d+)/(?P<idfranja>\d+)/$', order.buyOrder, name='buyOrder'),
    url(r'^consumers/$', consumer.createConsumer, name='createConsumer'),
    url(r'^consumers/complaint$', consumer.sendComplaint, name='sendComplaint'),

    url(r'^categories/$', category.queryAllCategories, name="queryAllCategories"),
    url(r'^categories/(?P<id>\d+)/$', category.queryCategoryById, name="queryCategoryById"),

    url(r'^franjas/actuales/$', franja.queryFranjaNextWeek, name='queryFranjaNextWeek'),
    url(r'^franjas/new/$', franja.createFranja, name='createFranja'),
    url(r'^franjas/delete/(?P<id_franja>\d+)/$', franja.deleteFranja, name='deleteFranja'),

    url(r'^baskets/$', basket.queryAllBaskets, name='queryAllBaskets'),
    url(r'^baskets/category/$', basket.queryBasketCategoryId, name='queryBasketCategoryId'),

    url(r'^basketItems/$', basketItem.createBasketItem, name='createBasketItem'),
    url(r'^basketItems/(?P<value>\d+)/$', basketItem.queryBasketItem, name='queryBasketItem'),

]
