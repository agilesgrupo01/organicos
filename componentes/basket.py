#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core import serializers
from django.http import HttpResponse
from .models import Producto, Categoria


# Función encargada de retrnar todos los productos de tipo canasta.
# Retorna un arreglo vacío en caso de que no exista ninguno.
def queryAllBaskets(request):
    if request.method == 'GET':
        try:
            categoria_canasta = Categoria.objects.filter(nombre='Canasta')
            products = []

            if categoria_canasta.count() != 0:
                canasta = categoria_canasta[0]
                products = Producto.objects.filter(idCategoria=canasta).order_by('nombre')

            return HttpResponse(serializers.serialize('json', products), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de retorna el id de categoría asociado al
# producto cansata.
def queryBasketCategoryId(request):
    if request.method == 'GET':
        categoria_canasta = Categoria.objects.filter(nombre='Canasta')

        if categoria_canasta.count() == 0:
            return HttpResponse("No existe una categoría 'Canasta' en la base de datos.", status=400)

        else:
            canasta_id = categoria_canasta[0].id
            return HttpResponse(json.dumps(canasta_id), content_type="application/json")
