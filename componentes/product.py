#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Jorge on 08/03/2016.

import datetime
import json
from collections import OrderedDict

import boto3
import os, math
from django.core import serializers
from django.http import HttpResponse

from componentes.tests_serviciosRest import imprimirAConsola
from .models import Producto, Categoria, Oferta, ItemOferta, ItemCanasta


# El servicio permite consultar la lista de todos productos que se pueden ofertar por un productor
def queryAllProducts(request):
    try:
        products = Producto.objects.all().order_by('nombre')
        return HttpResponse(serializers.serialize('json', products), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# El servicio permite consultar la lista de todos productos diferentes a tipo canasta
# registrados en la plataforma.
def queryAllNonBasketProducts(request):
    try:
        categoria_canasta = Categoria.objects.filter(nombre='Canasta')

        if categoria_canasta.count() != 0:
            canasta = categoria_canasta[0]
            products = Producto.objects.exclude(idCategoria=canasta).order_by('nombre')

        else:
            products = Producto.objects.order_by('nombre')
        return HttpResponse(serializers.serialize('json', products), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# El servicio permite consultar un roducto que se pueden ofertar por un productor
def queryProductById(request, value):
    try:
        response_data = []
        response_data_temp = {}
        products = Producto.objects.filter(id=value)

        for item in products:
            response_data_temp['id'] = item.id
            response_data_temp['idCategoria'] = item.idCategoria.id
            if (item.estado == '1'):
                response_data_temp['estado'] = "Activo"
            elif (item.estado == '0'):
                response_data_temp['estado'] = "Inactivo"
            else:
                response_data_temp['estado'] = item.estado

            response_data_temp['nombre'] = item.nombre
            response_data_temp['precio'] = item.precio
            response_data_temp['fechaRegistro'] = str(item.fechaRegistro)
            response_data_temp['foto'] = item.foto
            response_data_temp['calificacionPromedio'] = item.calificacionPromedio
            response_data_temp['unidad'] = item.unidad
            response_data.append(response_data_temp)
            response_data_temp = {}

        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de crear (POST) o modificar (PUT) un producto en la plataforma.
# TODO: crear modelo Unidad y reemplazar atributo
def createProduct(request):
    try:
        s3 = boto3.resource(
            's3',
            aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID', ''),
            aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY', ''))

        # Crear producto
        if request.method == 'POST':

            idCategoria = Categoria.objects.get(id=int(float(request.POST['idCategoria'])))
            estado = request.POST['estado']
            nombre = request.POST['nombre']
            precio = float(request.POST['precio'])
            foto = ""
            unidad = request.POST['unidad']

            productoDuplicado = Producto.objects.filter(nombre=nombre)

            if productoDuplicado and productoDuplicado.count() > 0:
                return HttpResponse("Ya existe un producto con el mismo nombre.", status=400)
            else:
                producto = Producto(idCategoria=idCategoria, nombre=nombre, estado=estado, precio=precio,
                                    foto=foto, unidad=unidad, calificacionPromedio=0)
                producto.save()

                imgNombreOriginal = request.FILES['file'].name
                imgExt = imgNombreOriginal[(imgNombreOriginal.rfind('.')): len(imgNombreOriginal)].lower()

                producto.foto = 'https://s3.amazonaws.com/organicos/productos/' + str(producto.id) + imgExt
                producto.save()
                s3.Bucket('organicos').put_object(Key='productos/' + str(producto.id) + imgExt, Body=request.FILES['file'])
                return HttpResponse(serializers.serialize('json', [producto]), content_type="application/json")
        elif request.method == 'GET':
            print("Consultando productos ofertados de la semana actual")
            productos = getProductosOfertadosJSON(getOfertasSemanaActual())
            print(productos)
            return HttpResponse(json.dumps(productos),
                                content_type="application/json", status=200)
    except Exception as e:
        print e.message
        return HttpResponse(json.dumps(e.message), content_type="application/json", status=400)


# Función encargada de editar y/o eliminar un producto.
def editProduct(request, value):
    try:
        s3 = boto3.resource(
            's3',
            aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID', ''),
            aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY', ''))

        # Crear producto
        if request.method == 'POST':
            print "editar"
            id = int(float(request.POST['id']))
            idCategoria = Categoria.objects.get(id=request.POST['idCategoria'])
            estado = request.POST['estado']
            nombre = request.POST['nombre']
            precio = float(request.POST['precio'])
            unidad = request.POST['unidad']

            producto = Producto.objects.get(id=id)

            if producto:
                producto.idCategoria = idCategoria
                producto.nombre = nombre
                producto.estado = estado
                producto.precio = precio
                producto.unidad = unidad

                if 'file' in request.FILES:
                    imgNombreOriginal = request.FILES['file'].name
                    imgExt = imgNombreOriginal[(imgNombreOriginal.rfind('.')): len(imgNombreOriginal)].lower()
                    producto.foto = 'https://s3.amazonaws.com/organicos/productos/' + str(id) + imgExt
                    s3.Bucket('organicos').put_object(Key='productos/' + str(producto.id) + imgExt, Body=request.FILES['file'])

                producto.save()

                return HttpResponse(serializers.serialize('json', [producto]), content_type="application/json")

        elif request.method == 'DELETE':
            product = Producto.objects.get(id=value)
            if product:
                product.delete()
                return HttpResponse(serializers.serialize('json', [product]), content_type="application/json")

            else:
                return HttpResponse("No se encontró un producto asociado.", content_type="application/json", status=400)

    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# El servicio permite consultar la lista de todos productos que se pueden ofertar por un productor
def queryAllProductsFilter(request):
    try:
        response_data = []
        response_data_temp = {}
        products = Producto.objects.order_by('nombre')
        actualDate = datetime.datetime.now()
        ofertas = Oferta.objects.all().filter(estado=1, fechaInicio__lte=actualDate,
                                              fechaFin__gte=actualDate).order_by('fechaRegistro')
        categoria_canasta = Categoria.objects.filter(nombre='Canasta')

        for item in products:
            response_data_temp['id'] = item.id
            response_data_temp['idCategoria'] = item.idCategoria.nombre
            if (item.estado == '1'):
                response_data_temp['estado'] = "Activo"
            elif (item.estado == '0'):
                response_data_temp['estado'] = "Inactivo"
            else:
                response_data_temp['estado'] = item.estado
            response_data_temp['nombre'] = item.nombre
            response_data_temp['precio'] = item.precio
            response_data_temp['fechaRegistro'] = str(item.fechaRegistro.strftime('%Y/%m/%d'))
            response_data_temp['foto'] = item.foto
            response_data_temp['calificacionPromedio'] = item.calificacionPromedio
            response_data_temp['unidad'] = item.unidad

            if item.idCategoria == categoria_canasta[0]:
                response_data_temp['cantidad'] = getBasketCant(item.id, ofertas)
            else:
                response_data_temp['cantidad'] = getCant(item.id, ofertas)

            response_data.append(response_data_temp)
            response_data_temp = {}

        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def getCant(idProducto, ofertas):
    try:
        canItm = 0
        for oferta in ofertas:
            itemOferta = ItemOferta.objects.all().filter(idOferta=oferta.id, idProducto=idProducto, cantidadActual__gt=0)
            for itemOf in itemOferta:
                canItm = canItm + itemOf.cantidadActual
        return canItm
    except Exception as e:
        print e


# Función encargada de calcular la cantidad disponible de canastas.
# Considera la cantidad mínima entre todos los ítems incluidos en la
# canasta.
def getBasketCant(idProducto, ofertas):
    try:
        basket = Producto.objects.get(id=idProducto)
        items = ItemCanasta.objects.filter(idCanasta=basket.id)
        min_cantidad_item = getCant(items[0].idProducto, ofertas)

        for item in items:
            cantidad = math.floor(getCant(item.idProducto, ofertas) / item.cantidad)

            if min_cantidad_item > cantidad:
                min_cantidad_item = cantidad

        return min_cantidad_item

    except Exception as e:
        print e


def getOfertasSemanaActual():
    currentDate = datetime.datetime.now().date()
    ofertas =  Oferta.objects.all().filter(fechaInicio__lte=currentDate) \
        .filter(fechaFin__gte=currentDate)
    imprimirAConsola("GET OFERTAS SEMANA ACTUAL",ofertas)
    return ofertas


# Devuelve los productos que estan siendo ofertados en las ofertas que se pasan como parametro en formato JSON (MOVIL)
def getProductosOfertadosJSON(ofertas):
    if ofertas.count() >= 1:
        lista_productos = []
        print('Ofertas')
        print(ofertas)
        print('Lista productos')
        print(ItemOferta.objects.filter(idOferta__in=ofertas).values_list('idProducto', flat=True))
        productos_ofertados = Producto.objects.filter(id__in=ItemOferta.objects.filter(idOferta__in=ofertas).values_list('idProducto', flat=True))
        for producto in productos_ofertados:
            stock = 0
            for item in ItemOferta.objects.filter(idOferta__in=ofertas).filter(idProducto=producto):
                stock = stock + item.cantidadActual
            print(producto.nombre)
            print(generateProductJSON(producto, stock))
            lista_productos.append(generateProductJSON(producto, stock))
        return lista_productos
    else:
        raise StandardError('No hay ofertas para la semana actual')


# Devuelve al mensaje json si el login fue exitoso (movil)
def generateProductJSON(producto, stock):
    producto_json = OrderedDict([('id', []), ('name', []), ('price', []), ('unit', []), ('premium', []), ('image', []), ('stock', [])])
    # producto_json = {'id':None,'name':None,'price':None,'unit':None,'premium':None,'image':None,'stock':None}
    producto_json['id'] = getUUID(producto.id)
    producto_json['name'] = producto.nombre
    producto_json['price'] = producto.precio
    producto_json['unit'] = producto.unidad
    producto_json['premium'] = producto.premium
    producto_json['image'] = producto.foto
    producto_json['stock'] = stock
    return producto_json


def getUUID(id):
    uuid = 'aaaa-11111-aaaa-'
    if id / 10 == 0:
        uuid = uuid + '000' + str(id)
    elif id / 10 == 2:
        uuid = uuid + '00' + str(id)
    elif id / 10 == 3:
        uuid = uuid + '0' + str(id)
    else:
        uuid = uuid + '' + str(id)
    return uuid


def getDisponibilidadProducto(producto):
    nextWeekDate = datetime.datetime.now()

    ofertas = Oferta.objects.all().filter(fechaInicio__lte=nextWeekDate, fechaFin__gte=nextWeekDate, estado=1)
    items = ItemOferta.objects.filter(idOferta__in=ofertas).filter(idProducto=producto)

    if items.count()<1:
        raise LookupError("ererofdad")
    cantidad_disponible = 0
    for item in items:
        cantidad_disponible = cantidad_disponible + item.cantidadActual

    return cantidad_disponible
