#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core import serializers
from django.http import HttpResponse
from .models import Categoria


# El servicio permite consultar la lista de todas las categorías registradas en la plataforma,
# excepto la categoría Canasta.
def queryAllCategories(request):
    try:
        if request.method == 'GET':
            categorias = Categoria.objects.exclude(nombre='Canasta')
            return HttpResponse(serializers.serialize('json', categorias), content_type="application/json")

    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def queryCategoryById(request, id):
    try:
        if request.method == 'GET':
            print id
            return HttpResponse(serializers.serialize('json', [Categoria.objects.get(id=id)]),
                                content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
