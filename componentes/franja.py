#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import json
from pprint import pprint

from django.core import serializers
from django.http import HttpResponse
from .models import Franja


# Devuelve las franjas de la siguiente semana
def queryFranjaNextWeek(request):
    if request.method == 'GET':
        # Se seleccionan el lunes y domingo de la siguiente semana
        next_week_day = datetime.datetime.now() + datetime.timedelta(7)
        week_day = next_week_day.weekday()
        fd = next_week_day - datetime.timedelta(week_day)
        first_day_next_week = fd.replace(fd.year, fd.month, fd.day, 0, 0, 0, 0, fd.tzinfo)
        ld = next_week_day + datetime.timedelta(6 - week_day)
        last_day_next_week = ld.replace(ld.year, ld.month, ld.day, 23, 59, 59, 999999, ld.tzinfo)
        print('Dias consultados')
        print(first_day_next_week)
        print(last_day_next_week)
        print('Dias consultados fin')

        try:
            franjas = Franja.objects.filter(fechaInicio__gte=first_day_next_week).filter(fechaFin__lte=last_day_next_week)
            franjasDAO = []
            for franja in franjas:
                franjasDAO.append(franjaToDAO(franja))
            print("Franjas consultadas")
            print(json.dumps(franjasDAO))
            return HttpResponse(json.dumps(franjasDAO), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def getDayName(value):
    dayName = 'Domingo'
    if value == 0:
        dayName = 'Domingo'
    elif value == 1:
        dayName = 'Lunes'
    elif value == 2:
        dayName = 'Martes'
    elif value == 3:
        dayName = 'Miercoles'
    elif value == 4:
        dayName = 'Jueves'
    elif value == 5:
        dayName = 'Viernes'
    elif value == 6:
        dayName = 'Sabado'

    return dayName


# Las franjas solo se pueden definir con horas enteras
def franjaToDAO(franja):
    franja_temp = {}
    franja_temp['id'] = franja.id
    franja_temp['nombreDia'] = getDayName(int(franja.fechaInicio.strftime("%w")))
    franja_temp['dia'] = getDayName(int(franja.fechaInicio.strftime("%w"))) + ' ' + franja.fechaInicio.strftime("%Y-%m-%d")
    franja_temp['numeroDia'] = franja.fechaInicio.weekday()
    franja_temp['inicioFranja'] = (franja.fechaInicio - datetime.timedelta(hours=5)).strftime('%H:%M')
    franja_temp['inicioFranjaInt'] = (franja.fechaInicio - datetime.timedelta(hours=5)).strftime('%H')
    franja_temp['finFranja'] = (franja.fechaFin - datetime.timedelta(hours=5)).strftime('%H:%M')
    franja_temp['finFranjaInt'] = (franja.fechaFin - datetime.timedelta(hours=5)).strftime('%H')
    return franja_temp


# Devuelve el DAO de la franja correspondiente al id que se pasa como parámetro
def getDAOFranja(request, id_franja):
    pprint("Buscando franja" + id_franja)
    try:
        franjaVista = []
        franja = franjaToDAO(Franja.objects.get(id=id_franja))
        franjaVista.append(franja)
        return HttpResponse(json.dumps(franjaVista), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


def deleteFranja(request, id_franja):
    print('Borrando franja' + id_franja)
    try:
        if request.method == 'DELETE':
            franja = Franja.objects.get(id=id_franja)
            franja.delete()
            return HttpResponse(content_type="application/json", status=200)
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Función encargada de crear un ítem de oferta.
def createFranja(request):
    print('Agregando franja')
    try:
        if request.method == 'POST':
            jsonFranja = json.loads(request.body)
            #idFranja = int(Franja.objects.latest('id').id) + 1
            #print('Id a anadir ' + str(idFranja))
            numeroDia = int(jsonFranja['numeroDia'])
            inicioFranjaInt = int(jsonFranja['inicioFranjaInt'])
            finFranjaInt = int(jsonFranja['finFranjaInt'])
            print('Numero de dia: ' + str(numeroDia))
            print('InicioFranjaInt: ' + str(inicioFranjaInt))
            print('FinFranjaInt: ' + str(finFranjaInt))

            next_week_day = datetime.datetime.now() + datetime.timedelta(7)
            week_day = next_week_day.weekday()
            fd = next_week_day - datetime.timedelta(week_day)
            fd_next = fd.replace(fd.year, fd.month, fd.day, 0, 0, 0, 0, fd.tzinfo)
            franja_date = fd_next + datetime.timedelta(numeroDia)

            fechaInicio = franja_date.replace(hour=inicioFranjaInt)
            fechaFin = franja_date.replace(hour=finFranjaInt)
            print('Fecha inicio' + str(fechaInicio))
            print('Fecha fin' + str(fechaFin))

            newFranja = Franja(tipo='PEDIDO', semana=0, fechaInicio=fechaInicio, fechaFin=fechaFin)
            newFranja.save()
            return HttpResponse(serializers.serialize('json', [newFranja]), content_type="application/json")
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
