# Created by Jorge on 08/03/2016.

import datetime
import json

from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Productor, Usuario


# El servicio permite consultar la lista de todos productores que se encuentran registrados en la aplicacion
@require_http_methods(["GET"])
def queryAllProducers(request):
    try:
        print request.method
        producers = Productor.objects.all()
        return HttpResponse(serializers.serialize('json', producers), content_type="application/json")
    except Exception as e:
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite registrar productores en la aplicacion
@require_http_methods(["POST"])
def createProducer(request):
    try:
        print request.method
        if request.method == 'POST':
            user = Usuario(
                correoElectronico=request.POST['correoElectronico'],
                contrasenia="1234",
                tipo="Productor",
                fechaRegistro=datetime.datetime.now()
            )
            user.save()
            producer = Productor(
                idUsuario=user,
                nombre=request.POST['nombre'],
                telefono=request.POST['telefono'],
                celular=request.POST['celular'],
                direccion=request.POST['direccion'],
                certificacion=request.POST['certificacion'],
                twitter=request.POST['twitter'],
                correoElectronico=request.POST['correoElectronico'],
                foto=request.POST['foto'],
                fechaRegistro=datetime.datetime.now()
            )
            producer.save()
            return HttpResponse(json.dumps(producer.id), content_type="application/json", status=200)
        else:
            return HttpResponse(json.dumps(-1), content_type="application/json", status=200)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)
