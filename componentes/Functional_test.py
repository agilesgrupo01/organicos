#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time
from unittest import TestCase

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException

SLEEP_TIME = 1
IMPLICITLY_WAIT_TIME = 10
ADMIN_USER = "admin@admin.com"
ADMIN_PASS = "1234"

PROD_USER = "prod@prod.com"
PROD_PASS = "1234"

CONS_USER = "cons@cons.com"
CONS_PASS = "1234"

CONS_NEW_USER = "cons@test.com"
CONS_NEW_PASS = "1234"

URL_PRODUCT_IMG = "C:\Users\Lina8a\Desktop\test.PNG"
URL_BASKET_IMG = "C:\Users\Lina8a\Desktop\test.PNG"


class FunctionalTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    # Función encargada de probar la inserción de un producto en la plataforma
    # TODO: definir aserción cuando se tenga tabla cn productos
    def test_product_create(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(1)
        self.browser.find_element_by_id('nav-login').click()

        self.browser.implicitly_wait(5)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('nav-products').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('add-product-form').click()

        self.browser.find_element_by_xpath("//select[@id='idCategoria']/option[text()='Carbohidrato']").click()

        nombre = self.browser.find_element_by_id('nombre')
        nombre.send_keys('AA')

        imagen = self.browser.find_element_by_id('file')
        imagen.send_keys(URL_PRODUCT_IMG)

        unidad = self.browser.find_element_by_id('unidad')
        unidad.send_keys('Kg')

        precio = self.browser.find_element_by_id('precio')
        precio.send_keys('100')

        self.browser.find_element_by_xpath("//select[@id='estado']/option[text()='Activo']").click()
        self.browser.find_element_by_id('submit-product').click()

        self.browser.implicitly_wait(10)
        producto = self.browser.find_element_by_id('product-AA')

        self.browser.implicitly_wait(10)
        self.assertEqual(producto.find_element_by_id("product-name").text, "Nombre : AA")

    # Función encargada de probar la edición de un producto en la plataforma
    def test_product_edit(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(1)
        self.browser.find_element_by_id('nav-login').click()

        self.browser.implicitly_wait(5)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('nav-products').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_xpath("//button[1]").click()

        self.browser.implicitly_wait(5)
        nombre = self.browser.find_element_by_id('nombre')
        nombre.clear()
        nombre.send_keys('A')

        unidad = self.browser.find_element_by_id('unidad')
        unidad.clear()
        unidad.send_keys('Kg')

        precio = self.browser.find_element_by_id('precio')
        precio.clear()
        precio.send_keys('100')

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('submit-product').click()

        self.browser.implicitly_wait(10)
        producto = self.browser.find_element_by_id('product-A')

        self.browser.implicitly_wait(10)
        self.assertEqual(producto.find_element_by_id("product-name").text, "Nombre : A")
        # self.assertEqual(producto.find_element_by_id("product-price").text, "Precio : 100")
        # self.assertEqual(producto.find_element_by_id("product-unit").text, "Unidad : Kg")

    # Función encargada de editar el plazo de confirmación de las ofertas.
    def test_edit_offer_window(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(1)
        self.browser.find_element_by_id('nav-login').click()

        self.browser.implicitly_wait(5)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('nav-offers-report').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('edit-offer-window').click()

        self.browser.find_element_by_xpath("//select[@id='dia']/option[text()='Lunes']").click()

        horaInicio = self.browser.find_element_by_id('horaInicio')
        horaInicio.clear()
        horaInicio.send_keys(4)

        horaFin = self.browser.find_element_by_id('horaFin')
        horaFin.clear()
        horaFin.send_keys(10)

        self.browser.find_element_by_id('submit-window').click()
        self.browser.implicitly_wait(5)

        self.assertEqual(self.browser.find_element_by_id('offer-window-text').text, 'Lunes de 4:00 a 10:00.')

    # Prueba para la funcionalidad de cierre de sesion (OR-14)
    def test_cierre_de_sesion(self):
        self.browser.get('http://localhost:8000/#/inicio')
        self.browser.implicitly_wait(1)

        # Inicio de sesion
        self.browser.find_element_by_id('nav-login').click()
        self.browser.implicitly_wait(1)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()
        self.browser.implicitly_wait(1)

        # Cierre de sesion
        self.browser.find_element_by_id('btn-cerrar-sesion').click()
        self.browser.implicitly_wait(1)

        # Se navega a inicio
        self.browser.get('http://localhost:8000/#/inicio')
        self.browser.implicitly_wait(1)

        # Se valida que los botones estan apropiadamente habilitados acorde a que el usuario esta fuera de la sesion
        self.assertEqual(self.browser.find_element_by_id('nav-login').is_displayed(), True)
        self.assertEqual(self.browser.find_element_by_id('btn-cerrar-sesion').is_displayed(), False)

    # Prueba Consultar todos los productores OR-52
    def test_producers_query(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_xpath("//a[text()='Productores']").click()
        self.browser.implicitly_wait(3)

        title = self.browser.find_element_by_xpath("//h2[text()='Lista Productores']").text
        self.assertIn("Lista Productores", title)

    # Función encargada de definir precio unificado de venta para un producto
    def test_unified_price(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(1)
        self.browser.find_element_by_id('nav-login').click()

        self.browser.implicitly_wait(5)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('nav-products').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_xpath("//button[1]").click()

        self.browser.implicitly_wait(5)
        nombre = self.browser.find_element_by_id('nombre')
        nombre.clear()
        nombre.send_keys('A')

        self.browser.implicitly_wait(5)
        precio = self.browser.find_element_by_id('precio')
        precio.clear()
        precio.send_keys('1000')

        self.browser.find_element_by_id('submit-product').click()
        self.browser.implicitly_wait(5)
        self.browser.get('http://localhost:8000/#/productos')

        self.browser.implicitly_wait(10)
        producto = self.browser.find_element_by_id('product-A')

        self.browser.implicitly_wait(10)
        self.assertEqual(producto.find_element_by_id("product-price").text, 'Precio : 1000')

    # Prueba Consultar datos de un productor OR-57
    def test_producers_detail(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_xpath("//a[text()='Productores']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_id('detail4').click()
        self.browser.implicitly_wait(3)

        self.assertIn(self.browser.find_element_by_id("producerLabel").text, "Productor")

    # Prueba para la funcionalidad de actualizacion de item de oferta (OR-41)
    # Precondiciones: Debe existir una oferta en estado -1 para el ultimo periodo asociada al usuario p@p.com
    # debe haber por lo menos un item asociado a la oferta.
    def test_actualizacion_item_oferta(self):
        self.browser.get('http://localhost:8000/login/')
        self.esperar()

        # Inicio de sesion
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(PROD_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(PROD_PASS)
        self.browser.find_element_by_id('but-login').click()
        self.esperar()

        # Se navega a inicio
        self.browser.get('http://localhost:8000/#/ofertas')
        self.esperar()

        # Se selecciona la ultima oferta en el menu de seleccion
        self.browser.find_element_by_xpath("//select[@id='idOfertasProductor']/option[last()]").click()
        self.esperar()

        # Se observan los items de la oferta
        self.browser.find_element_by_id('btnVerProductosOferta').click()
        self.esperar()

        # Se da click en la edicion del primer item de la oferta
        self.browser.find_element_by_xpath("//tbody[@id='tbodyItemsOferta']/tr[1]/th[1]/button[2]").click()
        self.esperar()

        # Setea la cantidad y el precio por unidad
        cantidad = random.randint(1, 200)
        precio = random.randint(1000, 10000)
        self.browser.find_element_by_id('cantidadOrigen').clear()
        self.browser.find_element_by_id('precio').clear()
        self.browser.find_element_by_id('btnModificarItemOferta').click()
        self.esperar()
        self.browser.find_element_by_id('cantidadOrigen').send_keys(cantidad)
        self.browser.find_element_by_id('precio').send_keys(precio)
        self.browser.find_element_by_id('btnModificarItemOferta').click()
        self.esperar()

        # Se verifica que los elementos en la tabla han sido correctamente actualizados
        self.assertEqual(self.browser.find_element_by_xpath("//tbody[@id='tbodyItemsOferta']/tr[1]/td[3]").text,
                         str(cantidad))
        self.assertEqual(self.browser.find_element_by_xpath("//tbody[@id='tbodyItemsOferta']/tr[1]/td[6]").text,
                         '$' + str(precio))

        # Cierre de sesion
        self.browser.find_element_by_id('btn-cerrar-sesion').click()
        self.browser.implicitly_wait(IMPLICITLY_WAIT_TIME)

    # Prueba para la funcionalidad de actualizacion de item de pedido (OR-45)
    # Precondiciones: Debe existir un pedido en estado 0 para el ultimo periodo asociado al usuario c@c.com
    # debe haber por lo menos un item asociado al pedido.
    def test_actualizacion_item_pedido(self):
        WebDriverWait(self.browser, 10)
        self.browser.get('http://localhost:8000/login/')
        self.browser.maximize_window()
        self.esperar()

        # Inicio de sesion
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_PASS)
        self.browser.find_element_by_id('but-login').click()
        self.esperar()

        # Se navega a inicio
        self.browser.get('http://localhost:8000/#/pedidos')
        self.esperar()

        # Se observan los items del pedido
        self.browser.find_element_by_id('btnVerProductosPedido').click()
        self.esperar()

        # Se da click en la edicion del primer item del pedido
        # Antes se almacena el id del item del pedido a actualizar
        idItemPedido = self.browser.find_element_by_xpath("//tbody[@id='tbodyItemsPedido']/tr[1]/td[1]").text
        print("IdItempedido " + str(idItemPedido))
        self.browser.find_element_by_xpath("//tbody[@id='tbodyItemsPedido']/tr[1]/th[1]/button[2]").click()
        self.esperar()

        # Setea la cantidad
        cantidad = random.randint(1, 200)
        self.browser.find_element_by_id('cantidad').clear()
        self.esperar()
        self.browser.find_element_by_id('cantidad').send_keys(cantidad)
        self.browser.find_element_by_id('btnModalAdd').click()
        self.esperar()

        # Debido a que al actualizar algunas veces el registro cambia de lugar (de numero de fila) es necesario validar su posicion
        numeroFila = self.browser.execute_script(
            "var bodyTable = document.getElementById('tbodyItemsPedido');var rows = bodyTable.children;for (var i = 0; i < rows.length; i++) {var id = (rows[i].children)[0].innerText;if(id==" + idItemPedido + "){return i;}}return -1;")
        numeroFila = numeroFila + 1
        print('Numero fila del registro actualizado: ' + str(numeroFila))
        WebDriverWait(self.browser, 10).until(EC.text_to_be_present_in_element(
            (By.XPATH, "//tbody[@id='tbodyItemsPedido']/tr[" + str(numeroFila) + "]/td[6]"), str(cantidad)))

        # Se verifica que la cantidad en la tabla ha sido correctamente actualizada
        self.assertEqual(self.browser.find_element_by_xpath(
            "//tbody[@id='tbodyItemsPedido']/tr[" + str(numeroFila) + "]/td[6]").text, str(cantidad))

        # Cierre de sesion
        self.browser.find_element_by_id('btn-cerrar-sesion').click()
        self.browser.implicitly_wait(IMPLICITLY_WAIT_TIME)

    # Prueba para la funcionalidad de definicion de fechas del pedido (OR-21)
    # Precondiciones: Debe estar creado el usuario a@a.com, no debe haber ninguna franja para la siguiente semana
    def test_definicion_de_pedido(self):
        WebDriverWait(self.browser, 10)
        self.browser.maximize_window()
        self.browser.get('http://localhost:8000/login/')
        self.esperar()

        # Inicio de sesion
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys('a@a.com')
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys('1234')
        self.browser.find_element_by_id('but-login').click()
        self.esperar()

        # Se navega a inicio
        self.browser.get('http://localhost:8000/#/franjas')
        self.esperar()

        # Se agrega un franja y se verifica que aparezca en la tabla de franjas
        self.browser.find_element_by_id('btnAgregar').click()
        self.esperar()

        # Se ingresan los datos de la franja a agregar y se da click en agregar
        self.browser.find_element_by_xpath("//select[@id='idDia']/option[last()]").click()
        self.esperar()
        self.browser.find_element_by_xpath("//select[@id='idHoraInicio']/option[text()='10:00']").click()
        self.esperar()
        self.browser.find_element_by_xpath("//select[@id='idHoraFin']/option[text()='12:00']").click()
        self.esperar()
        print('Texto boton: ' + self.browser.find_element_by_id('btnAgregarFranja').text)
        self.browser.find_element_by_id('btnAgregarFranja').submit()
        self.esperar()

        # Se verifica que la cantidad en la tabla ha sido correctamente actualizadas
        self.assertEqual(self.browser.find_element_by_xpath("//tbody[@id='tbodyFranjas']/tr[last()]/td[2]").text,
                         'Domingo')
        self.assertEqual(self.browser.find_element_by_xpath("//tbody[@id='tbodyFranjas']/tr[last()]/td[3]").text,
                         '10:00')
        self.assertEqual(self.browser.find_element_by_xpath("//tbody[@id='tbodyFranjas']/tr[last()]/td[4]").text,
                         '12:00')

        # Cierre de sesion
        self.browser.find_element_by_id('btn-cerrar-sesion').click()
        self.browser.implicitly_wait(IMPLICITLY_WAIT_TIME)

    def esperar(self):
        self.browser.implicitly_wait(IMPLICITLY_WAIT_TIME)
        time.sleep(SLEEP_TIME)

    # Prueba Consultar datos de productos OR-56
    def test_products_all(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_xpath("//a[text()='Productos']").click()
        self.browser.implicitly_wait(3)

        self.assertIsNotNone(self.browser.find_element_by_id("product-name").text)
        self.assertIsNotNone(self.browser.find_element_by_id("product-price").text)
        self.assertIsNotNone(self.browser.find_element_by_id("product-unit").text)
        nombre = self.browser.find_element_by_id('product-name').text
        precio = self.browser.find_element_by_id('product-price').text
        unit = self.browser.find_element_by_id('product-unit').text
        self.assertIn('Nombre :', nombre)
        self.assertIn('Precio :', precio)
        self.assertIn('Unidad :', unit)

    # Prueba Consultar detalle de un productos OR-65
    def test_products_detail(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_xpath("//a[text()='Productos']").click()
        self.browser.implicitly_wait(3)

        nombre = self.browser.find_element_by_id('product-name').text
        precio = self.browser.find_element_by_id('product-price').text
        unit = self.browser.find_element_by_id('product-unit').text

        self.browser.find_element_by_id("product-photo").click()
        self.browser.implicitly_wait(3)

        self.assertIsNotNone(self.browser.find_element_by_id("categoria").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("estado").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("nombre").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("precio").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("fechaRegistro").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("calificacionPromedio").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("unidad").get_attribute('value'))

        nombre2 = 'Nombre : ' + self.browser.find_element_by_id('nombre').get_attribute('value')
        precio2 = 'Precio : ' + self.browser.find_element_by_id('precio').get_attribute('value')
        unit2 = 'Unidad : ' + self.browser.find_element_by_id('unidad').get_attribute('value')

        self.assertEqual(nombre, nombre2)
        self.assertEqual(precio, precio2)
        self.assertEqual(unit, unit2)

    # Prueba Consultar detalle de un productos OR-46 debe existir el producto Papa
    def test_order_products_detail(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.esperar()

        self.browser.find_element_by_xpath("//a[text()='Mis Pedidos']").click()
        self.esperar()

        self.browser.find_element_by_id("btnVerProductosPedido").click()
        self.esperar()

        self.browser.find_element_by_id("btAgregar").click()
        self.esperar()

        self.browser.find_element_by_id("producto").click()
        self.esperar()

        self.browser.find_element_by_id('producto').send_keys('Papa')
        self.esperar()

        self.browser.find_element_by_id('cantidad').send_keys(1)
        self.esperar()

        self.browser.find_element_by_id("btnModalAdd").click()
        self.esperar()

        self.browser.find_element_by_id("btDet").click()
        self.browser.implicitly_wait(3)

        self.assertIsNotNone(self.browser.find_element_by_id("categoria").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("estado").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("nombre").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("precio").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("fechaRegistro").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("calificacionPromedio").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("unidad").get_attribute('value'))

        nombre = self.browser.find_element_by_id('nombre').get_attribute('value')
        cantidad = self.browser.find_element_by_id('cantidad').get_attribute('value')

        self.assertEqual(nombre, 'Papa')
        self.assertEqual(cantidad, '1')

    # Filtrar reporte por producto OR-48 debe haber por lo menos una oferta con el producto Papa
    def test_reporte_prod(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.esperar()

        self.browser.find_element_by_xpath("//a[text()='Reporte Ofertas']").click()
        self.esperar()

        self.browser.find_element_by_id("selProd").click()
        self.esperar()

        self.browser.find_element_by_id('selProd').send_keys('Papa')
        self.esperar()

        self.browser.find_element_by_id("genRep").click()
        self.esperar()

        nombre = self.browser.find_element_by_id('nombreProd').text
        self.assertEqual(nombre, 'Papa')

    # Prueba encargada de validar el registro de un consumidor
    def test_consumer_create(self):
        self.browser.get('http://localhost:8000/#/registro/')
        self.browser.implicitly_wait(3)

        nombres = self.browser.find_element_by_id('nombres')
        nombres.send_keys("Prueba")

        apellidos = self.browser.find_element_by_id('apellidos')
        apellidos.send_keys("Consumidor")

        telefono = self.browser.find_element_by_id('telefono')
        telefono.send_keys("2000000")

        direccion = self.browser.find_element_by_id('direccion')
        direccion.send_keys("Cra prueba # 10 - 20")

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_NEW_USER)

        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_NEW_PASS)
        self.esperar()

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(50)
        self.esperar()

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_NEW_USER)

        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_NEW_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(50)
        self.esperar()

        self.assertEqual(self.browser.find_element_by_id("nav-mis-pedidos").is_displayed(), True)

    # Prueba para diligenciar y enviar quejas y reclamos OR-34
    def test_send_complaint(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(5)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_PASS)
        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_xpath("//a[text()='Quejas y Reclamos']").click()
        self.browser.implicitly_wait(5)

        self.browser.find_element_by_id("subject").send_keys("subject")
        self.browser.find_element_by_id("observation").send_keys("observation")

        self.browser.find_element_by_id("sendComplaint").click()
        self.browser.implicitly_wait(10)

        message = self.browser.find_element_by_id("message").text
        self.browser.implicitly_wait(5)

        self.assertIn("satisfactoriamente", message)

    # Prueba la correcta creación de una canasta.
    def test_basket_create(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(1)
        self.browser.find_element_by_id('nav-login').click()

        self.browser.implicitly_wait(5)
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('nav-canastas').click()

        self.browser.implicitly_wait(5)
        self.browser.find_element_by_id('add-basket-form').click()

        nombre = self.browser.find_element_by_id('nombre')
        nombre.send_keys('Canasta A')

        imagen = self.browser.find_element_by_id('file')
        imagen.send_keys(URL_BASKET_IMG)

        precio = self.browser.find_element_by_id('precio')
        precio.send_keys('0')

        self.browser.find_element_by_xpath("//select[@id='estado']/option[text()='Activo']").click()
        self.browser.find_element_by_id('submit-basket').click()

        self.browser.implicitly_wait(10)
        producto = self.browser.find_element_by_id('product-Canasta-A')

        self.browser.implicitly_wait(10)
        self.assertEqual(producto.find_element_by_id("product-name").text, "Nombre : Canasta A")

    # Filtrar pedido por producto OR-74 debe haber por lo menos una oferta con el producto Papa
    def test_reporte_ofer(self):
        self.browser.get('http://localhost:8000/login/')
        self.browser.implicitly_wait(3)

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(CONS_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(CONS_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.esperar()

        self.browser.find_element_by_xpath("//a[text()='Mis Pedidos']").click()
        self.esperar()

        self.browser.find_element_by_id("btAgregar").click()
        self.esperar()

        self.browser.find_element_by_xpath("//input[@name='{{name}}']").send_keys('Papa')
        self.esperar()

        nombre = self.browser.find_element_by_id('filterName').text
        self.assertEqual(nombre, 'Papa')

    # Prueba encargada de validar la creacion de ítems de canasta.
    # TODO: agregar aserción después de que se visualicen los productos
    def test_basket_item_create(self):
        self.browser.get('http://localhost:8000')
        self.esperar()
        self.browser.find_element_by_id('nav-login').click()

        self.esperar()
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.esperar()
        self.browser.find_element_by_id('nav-canastas').click()

        self.esperar()
        self.browser.find_element_by_id('but-product-Canasta-A').click()

        self.esperar()
        self.browser.find_element_by_xpath("//select[@id='producto']/option[last()]").click()
        cantidad = self.browser.find_element_by_id('cantidad')
        cantidad_val = random.randint(1, 20)
        cantidad.send_keys(str(cantidad_val))

        self.esperar()
        self.browser.find_element_by_id('btnAddBasketItem').click()


    # Prueba Consultar detalle de una canasta OR-67
    def test_products_basket_detail(self):
        self.browser.get('http://localhost:8000')
        self.esperar()
        self.browser.find_element_by_id('nav-login').click()

        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)

        self.browser.find_element_by_xpath("//input[@type='submit']").click()
        self.browser.implicitly_wait(3)

        self.browser.find_element_by_xpath("//a[text()='Canastas']").click()
        self.browser.implicitly_wait(3)

        nombre = self.browser.find_element_by_id('product-name').text
        precio = self.browser.find_element_by_id('product-price').text


        self.browser.find_element_by_id("product-photo").click()
        self.browser.implicitly_wait(3)

        self.assertIsNotNone(self.browser.find_element_by_id("estado").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("nombre").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("precio").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("fechaRegistro").get_attribute('value'))
        self.assertIsNotNone(self.browser.find_element_by_id("calificacionPromedio").get_attribute('value'))


        nombre2 = 'Nombre : ' + self.browser.find_element_by_id('nombre').get_attribute('value')
        precio2 = 'Precio : ' + self.browser.find_element_by_id('precio').get_attribute('value')

        self.assertEqual(nombre, nombre2)
        self.assertEqual(precio, precio2)


    # Prueba que valida la eliminación de un producto.
    def test_product_delete(self):
        self.browser.get('http://localhost:8000')
        self.esperar()
        self.browser.find_element_by_id('nav-login').click()

        self.esperar()
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.esperar()
        self.browser.find_element_by_id('nav-products').click()

        self.esperar()
        self.browser.find_element_by_id('add-product-form').click()

        self.browser.find_element_by_xpath("//select[@id='idCategoria']/option[last()]").click()

        nombre = self.browser.find_element_by_id('nombre')
        nombre.send_keys('A')

        imagen = self.browser.find_element_by_id('file')
        imagen.send_keys(URL_PRODUCT_IMG)

        unidad = self.browser.find_element_by_id('unidad')
        unidad.send_keys('Kg')

        precio = self.browser.find_element_by_id('precio')
        precio.send_keys('100')

        self.browser.find_element_by_xpath("//select[@id='estado']/option[text()='Activo']").click()
        self.browser.find_element_by_id('submit-product').click()

        self.esperar()
        self.browser.find_element_by_id('but-delete-product-A').click()

        self.esperar()
        self.browser.find_element_by_id('btn-modal-delete').click()

        self.esperar()

        try:
            self.browser.find_element_by_id('product-A')
        except NoSuchElementException:
            self.assertTrue(True)


    # Prueba que valida la eliminación de un producto.
    def test_basket_delete(self):
        self.browser.get('http://localhost:8000')
        self.esperar()
        self.browser.find_element_by_id('nav-login').click()

        self.esperar()
        correoElectronico = self.browser.find_element_by_id('correoElectronico')
        correoElectronico.send_keys(ADMIN_USER)
        contrasenia = self.browser.find_element_by_id('contrasenia')
        contrasenia.send_keys(ADMIN_PASS)
        self.browser.find_element_by_id('but-login').click()

        self.esperar()
        self.browser.find_element_by_id('nav-canastas').click()

        self.esperar()
        self.browser.find_element_by_id('add-basket-form').click()

        nombre = self.browser.find_element_by_id('nombre')
        nombre.send_keys('CanastaA')

        imagen = self.browser.find_element_by_id('file')
        imagen.send_keys(URL_PRODUCT_IMG)

        precio = self.browser.find_element_by_id('precio')
        precio.send_keys('100')

        self.browser.find_element_by_xpath("//select[@id='estado']/option[text()='Activo']").click()
        self.browser.find_element_by_id('submit-basket').click()

        self.esperar()
        self.browser.find_element_by_id('but-delete-product-CanastaA').click()

        self.esperar()
        self.browser.find_element_by_id('btn-modal-delete').click()

        self.esperar()

        try:
            self.browser.find_element_by_id('product-CanastaA')
        except NoSuchElementException:
            self.assertTrue(True)