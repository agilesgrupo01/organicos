#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse
from django.shortcuts import render
from .models import Usuario

# Create your views here.

# Método encargado de renderizar la página de inicio del proyecto.
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, "componentes/index.html")


# Despliega la vista para el inicio de sesión
def login(request):
    return render(request, 'componentes/login.html')


@csrf_exempt
def post_login(request):
    try:
        freelance = Usuario.objects.get(correoElectronico=request.POST['correoElectronico'])
        datos = {'id': freelance.id, 'rol': freelance.tipo}
        if (freelance.contrasenia == request.POST['contrasenia']):
            return HttpResponse(json.dumps(datos), content_type="application/json", status=200)
        else:
            return HttpResponse(json.dumps(freelance.correoElectronico + " " + freelance.contrasenia),
                                content_type="application/json", status=401)
    except Exception as e:
        print(e.message)
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


@csrf_exempt
def login_rest(request):
    received_json_data = json.loads(request.body.decode("utf-8"))
    print("request body: " + received_json_data["username"])
    print("request body: " + received_json_data["password"])
    try:
        usuario = Usuario.objects.get(correoElectronico=received_json_data["username"])
        if (usuario.contrasenia == received_json_data["password"]):
            print(generateLoginSuccesfulJSON(received_json_data["username"]))
            return HttpResponse(json.dumps(generateLoginSuccesfulJSON(received_json_data["username"])), content_type="application/json", status=200)
        else:
            print(generateLoginFailedJSON(received_json_data["username"]))
            return HttpResponse(json.dumps(generateLoginFailedJSON(received_json_data["username"])),
                                content_type="application/json", status=200)
    except Exception as e:
        print(e.message)
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Devuelve al mensaje json si el login fue exitoso (movil)
def generateLoginSuccesfulJSON(username):
    login_ok_json = {}
    login_ok_json['username'] = username
    login_ok_json['status'] = 'OK'
    login_ok_json['message'] = 'El usuario ha iniciado sesión'
    return login_ok_json


# Devuelve al mensaje json si el login fue fallido (movil)
def generateLoginFailedJSON(username):
    login_fail_json = {}
    login_fail_json['username'] = username
    login_fail_json['status'] = 'ERROR'
    login_fail_json['message'] = 'Usuario o Clave incorrecta'
    return login_fail_json
