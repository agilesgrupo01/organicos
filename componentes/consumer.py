#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

import boto3
import os
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from .mail import Mail
from .models import Usuario, Consumidor


# Función encargada de crear un nuevo consumidor en la plataforma.
# Se valida que no exista otro consumidor registrado con el mismo
# correo electróncio.
def createConsumer(request):
    s3 = boto3.resource(
        's3',
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID', ''),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY', ''))
    try:
        if request.method == 'POST':
            nombres = request.POST['nombres']
            apellidos = request.POST['apellidos']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            correoElectronico = request.POST['correoElectronico']
            contrasenia = request.POST['contrasenia']
            tipo = 'Consumidor'

            usuarios = Usuario.objects.filter(correoElectronico=correoElectronico)

            for u in usuarios:
                if u.tipo == tipo:
                    return HttpResponse("Otro consumidor ya se ha registrado con el mismo correo electrónico.",
                                        status=400)

            usuario_nuevo = Usuario(correoElectronico=correoElectronico, contrasenia=contrasenia, tipo=tipo)
            usuario_nuevo.save()

            if 'file' in request.FILES:
                imgNombreOriginal = request.FILES['file'].name
                imgExt = imgNombreOriginal[(imgNombreOriginal.rfind('.')): len(imgNombreOriginal)].lower()
                foto = 'https://s3.amazonaws.com/organicos/consumidores/' + str(usuario_nuevo.id) + imgExt
                s3.Bucket('organicos').put_object(Key='consumidores/' + str(usuario_nuevo.id) + imgExt,
                                                  Body=request.FILES['file'])
            else:
                foto = ''

            consumidor_nuevo = Consumidor(nombres=nombres, apellidos=apellidos, telefono=telefono, direccion=direccion,
                                          foto=foto, id_usuario=usuario_nuevo)
            consumidor_nuevo.save()
            return HttpResponse(serializers.serialize('json', [consumidor_nuevo]), content_type="application/json")

    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Permite enviar la informacion de peticiones y quejas
@require_http_methods(["POST"])
def sendComplaint(request):
    try:
        if request.method == 'POST':
            to = getAdminMail()
            cc = ""
            if request.POST['copy']:
                user = Usuario.objects.get(id=request.POST['userId'])
                cc = user.correoElectronico
            mail = Mail(to, cc, request.POST['subject'], request.POST['observation'])
            mail.send()
            return HttpResponse(json.dumps(""), content_type="application/json", status=200)
        else:
            return HttpResponse(json.dumps("Envío no permitido"), content_type="application/json", status=400)
    except Exception as e:
        print e
        return HttpResponse(json.dumps(e.args), content_type="application/json", status=400)


# Obtiene el correo del usuario administrador
def getAdminMail():
    admins = Usuario.objects.filter(tipo="Administrador")
    if len(admins) > 0:
        return admins[0]
    else:
        return None
