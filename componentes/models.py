from __future__ import unicode_literals

from django.db import models


# JD.RUNZA -050316:/Modelo Proyecto Organicos

class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    correoElectronico = models.EmailField(max_length=50, unique=True)
    contrasenia = models.CharField(max_length=15)
    tipo = models.CharField(max_length=15, null=False, blank=False)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)


class Consumidor(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=50, null=False, blank=False)
    apellidos = models.CharField(max_length=50, null=False, blank=False)
    telefono = models.IntegerField(null=True, blank=True)
    foto = models.CharField(max_length=1000, null=True, blank=True)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)
    direccion = models.CharField(max_length=255, null=True, blank=True)
    id_usuario = models.ForeignKey(Usuario, null=False, blank=False)


class Productor(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False)
    telefono = models.CharField(max_length=50, null=False, blank=False)
    celular = models.CharField(max_length=50, null=False, blank=False)
    foto = models.TextField()
    certificacion = models.CharField(max_length=50, null=False, blank=False)
    twitter = models.CharField(max_length=15, null=True, blank=True)
    correoElectronico = models.EmailField(max_length=50, unique=True)
    direccion = models.CharField(max_length=255, null=True, blank=True)
    idUsuario = models.ForeignKey(Usuario, null=False, blank=False)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)


class Categoria(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False)
    descripcion = models.CharField(max_length=255, null=True, blank=True)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)


class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    idCategoria = models.ForeignKey(Categoria, null=False, blank=False)
    estado = models.CharField(max_length=30, null=False, blank=False)
    nombre = models.CharField(max_length=50, null=False, blank=False)
    precio = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)
    foto = models.CharField(max_length=1000, null=True, blank=True)
    calificacionPromedio = models.IntegerField(null=False, blank=False, default=0)
    unidad = models.CharField(max_length=10, null=True, blank=True)
    premium = models.NullBooleanField()


class Franja(models.Model):
    id = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=100, null=False, blank=False)
    semana = models.IntegerField(null=False, blank=False)
    fechaInicio = models.DateTimeField(blank=False)
    fechaFin = models.DateTimeField(blank=False)


class Pedido(models.Model):
    id = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=30, null=False, blank=False)
    idConsumidor = models.ForeignKey(Consumidor)
    precioTotal = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)
    idFranja = models.ForeignKey(Franja, null=True, blank=True)


class ItemPedido(models.Model):
    id = models.AutoField(primary_key=True)
    idPedido = models.ForeignKey(Pedido)
    idProducto = models.ForeignKey(Producto)
    cantidad = models.IntegerField()
    precio = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)


class Oferta(models.Model):
    id = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=30, null=False, blank=False)
    idProductor = models.ForeignKey(Productor)
    precioTotal = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)
    fechaInicio = models.DateField(blank=False, null=True)
    fechaFin = models.DateField(blank=False, null=True)


class ItemOferta(models.Model):
    id = models.AutoField(primary_key=True)
    idOferta = models.ForeignKey(Oferta)
    idProducto = models.ForeignKey(Producto)
    cantidadOrigen = models.IntegerField(null=False, blank=False, default=0)
    cantidadActual = models.IntegerField(null=False, blank=False, default=0)
    precio = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)


class ListaValores(models.Model):
    id = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=100, null=False, blank=False)
    codigo = models.CharField(max_length=100, null=False, blank=False)
    valor = models.CharField(max_length=100, null=False, blank=False)
    idPadre = models.IntegerField()


class ItemCanasta(models.Model):
    id = models.AutoField(primary_key=True)
    idCanasta = models.ForeignKey(Producto, related_name='canasta')
    idProducto = models.ForeignKey(Producto, related_name='producto')
    cantidad = models.IntegerField()
    precio = models.FloatField(null=False, blank=False, default=0)
    fechaRegistro = models.DateTimeField(auto_now_add=True, blank=True)
