# Orgánicos
El presente repositorio tiene el código asociado al proyecto Orgánicos del grupo 1 del curso "Procesos de desarrollo ágiles" 2016-10.

## Heroku

Dearrollo: https://organicos.herokuapp.com/
Producción: https://organico.herokuapp.com/


## CodeShip
A continuación se muestra el estado de despliegue de los branch de desarrollo y producción:

Desarrollo: [ ![Codeship Status for agilesgrupo01/organicos](https://codeship.com/projects/c2acff60-e806-0133-0286-429aaf3cc23f/status?branch=develop)](https://codeship.com/projects/147073)
Producción: [ ![Codeship Status for agilesgrupo01/organicos](https://codeship.com/projects/c2acff60-e806-0133-0286-429aaf3cc23f/status?branch=master)](https://codeship.com/projects/147073)


## Codacy
A continuación se muestra la calificación de código estático efectuada por Codacy:


[![Codacy Badge](https://api.codacy.com/project/badge/Grade/af9d7f626b7a4ea59561936f59ef5b06)](https://www.codacy.com/app/jorge-e-barrera/organicos?utm_source=agilesgrupo01@bitbucket.org&amp;utm_medium=referral&amp;utm_content=agilesgrupo01/organicos&amp;utm_campaign=Badge_Grade)

